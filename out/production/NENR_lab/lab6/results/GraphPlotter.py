import matplotlib.pyplot as plt


if __name__ == "__main__":


    fig = plt.figure(figsize=(8, 6))
    for n_rules in (1, 2, 10):
        with open("results(" + str(n_rules) + ").txt", "r") as opened:
            lines = opened.read().split("\n")
            domain = []
            codomain = []
            for l in lines:
                if len(domain) > 1000:
                    break
                if len(l) > 2:
                    data = l.split()
                    domain.append(float(data[1]))
                    codomain.append(float(data[5]))

            plt.plot(domain, codomain)

    plt.legend([("Rules = " + str(i)) for i in (1, 2, 10)])
    plt.show()
