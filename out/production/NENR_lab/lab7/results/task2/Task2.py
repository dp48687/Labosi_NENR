import matplotlib.pyplot as plt


def load_data(path):
    with open(path, "r") as opened:
        content = opened.read()
        data = {}
        for line in content.split("\n"):
            values = line.split("\t")
            class_label = values[2] + values[3] + values[4]
            try:
                data[class_label][0].append(float(values[0]))
                data[class_label][1].append(float(values[1]))
            except KeyError:
                data[class_label] = [[float(values[0])], [float(values[1])]]
        return data


if __name__ == "__main__":
    dataset = load_data("zad7-dataset.txt")

    markers = ["D", "s", "P", "v", "o"]

    figure = plt.figure("Dataset", figsize=(10,4.5))
    i = 0
    for class_label in list(dataset.keys()):
        data = dataset[class_label]
        plt.scatter(data[0], data[1], marker=markers[i % len(markers)])
        i += 1
    plt.legend(list(dataset.keys()))
    plt.xlabel("X1", labelpad=2)
    plt.ylabel("X2", labelpad=20, rotation=0)

    plt.show()
