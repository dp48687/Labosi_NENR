import matplotlib.pyplot as plt


def argmax(values):
    max_index = 0
    for i, val in enumerate(values):
        if val > values[max_index]:
            max_index = i
    return max_index


def color(values):
    ind = argmax(values)
    if ind == 0:
        return "#FF0000"
    elif ind == 1:
        return "#00FF00"
    else:
        return "#0000FF"


if __name__ == "__main__":
    # data = load_data(pth.join("classicga", "overall.txt"))
    figure = plt.figure("Task 4 - generational GA, simple gaussian mutation", figsize=(10, 4.5))
    cmap = plt.cm.Spectral
    indices = [(i * 2000 + j) for i in range(2000) for j in range(2000) if i % 20 == 0 and j % 20 == 0]
    with open("overall.txt", "r") as opened:
        content = opened.read()
        for i, line in enumerate(content.split("\n")):
            if i in indices:
                print(i)
                values = line.split()
                if len(values) > 4:
                    plt.scatter(float(values[0]), float(values[1]),
                                c=color((float(values[2]), float(values[3]), float(values[4]))),
                                s=1
                    )
    plt.xlabel("$X_1$", labelpad=2)
    plt.ylabel("$X_2$", labelpad=20, rotation=0)

    plt.show()
