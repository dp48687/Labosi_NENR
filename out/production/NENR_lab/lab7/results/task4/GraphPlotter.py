import matplotlib.pyplot as plt


if __name__ == "__main__":
    strings = "[0.1284698535082189, 0.8742248452931859, 0.6220212312828246, 0.8703392889614802, 0.11847615180759705, 0.6314716110484782, 0.3731641072910329, 0.14501084195117966, 0.29478962188144775, 0.7419053051645765, 0.7402992995143279, 0.26381819744208296, 0.739402709558259, 0.26058211001049963, 0.25966019193042844, 0.2553706770824994, 23.668702164062292, -11.990314458638517, -4.626709547177901, -79.77813477626651, -6.270798177104636, -12.687225927553168, 56.33365594209858, -15.519814882134543, -7.568993047066587, 58.81227498577786, -17.308213763484893, -6.767430992411892, -70.83361668822283, -9.940510818144945, -0.7180983704261483, -79.2756769173534, -4.818414586139452, -4.174182327515715, -62.979048330790945, -8.782551950462341, -8.497578144046852, 20.347121559386956, -16.851704738358933, -15.968260193308145, -1.3515723945496105, 7.911042642610477, 13.59361703989595, -0.49152503595397096, 0.6366634537430491, -0.07305441796460582, -0.19196852560951055, 0.17831371147617886, -0.34123273228422507, -0.1270444093531647, -0.2302497396405191, 0.07229321962098012, 0.11320392331888275, -0.12803851771024127, -0.17159942859871355, -0.051537671904041515, 619.615155455828, 0.35112687853266594, -0.6964720239618133]" \
                  .replace(",", "").replace("[", "").replace("]", "").split()[0:16]

    floats = [float(strings[i]) for i in range(16)]


    def load_data(path):
        with open(path, "r") as opened:
            content = opened.read()
            data = {}
            for line in content.split("\n"):
                values = line.split("\t")
                class_label = values[2] + values[3] + values[4]
                try:
                    data[class_label][0].append(float(values[0]))
                    data[class_label][1].append(float(values[1]))
                except KeyError:
                    data[class_label] = [[float(values[0])], [float(values[1])]]
            return data


    dataset = load_data("zad7-dataset.txt")
    markers = ["D", "s", "P", "v", "o"]
    figure = plt.figure("Dataset", figsize=(10, 4.5))

    for i in range(8):
        print(floats[i], floats[i + 8])
        plt.scatter(floats[i], floats[i + 8], color="#000000")

    i = 0
    for class_label in list(dataset.keys()):
        data = dataset[class_label]
        plt.scatter(data[0], data[1], marker=markers[i % len(markers)])
        i += 1
    plt.xlabel("X1", labelpad=2)
    plt.ylabel("X2", labelpad=20, rotation=0)
    plt.show()
