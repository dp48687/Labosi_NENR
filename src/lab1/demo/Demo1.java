package lab1.demo;

import lab1.domain.AbstractDomain;
import lab1.domain.Debug;
import lab1.domain.DomainElement;
import lab1.domain.IDomain;

public class Demo1 {

    public static void main(String[] args) {
        IDomain d1 = AbstractDomain.intRange(0, 5); // {0,1,2,3,4}
        Debug.print(d1, "Elementi domene d1:");
        IDomain d2 = AbstractDomain.intRange(0, 3); // {0,1,2}
        Debug.print(d2, "Elementi domene d2:");
        IDomain d3 = AbstractDomain.combine(d1, d2);
        Debug.print(d3, "Elementi domene d3:");
        System.out.println(d3.elementForIndex(0));
        System.out.println(d3.elementForIndex(5));
        System.out.println(d3.elementForIndex(14));
        System.out.println(d3.indexOf(DomainElement.of(new int[]{4, 1})));
    }

}
