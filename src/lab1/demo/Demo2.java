package lab1.demo;

import lab1.domain.AbstractDomain;
import lab1.domain.Debug;
import lab1.domain.DomainElement;
import lab1.domain.IDomain;
import lab1.fuzzy.CalculatedFuzzySet;
import lab1.fuzzy.IFuzzySet;
import lab1.fuzzy.MutableFuzzySet;
import lab1.fuzzy.StandardFuzzySets;

public class Demo2 {

    public static void main(String[] args) {
        IDomain d = AbstractDomain.intRange(0, 11);
        IFuzzySet set1 = new MutableFuzzySet(d)
                .set(DomainElement.of(new int[]{0}), 1.0)
                .set(DomainElement.of(new int[]{1}), 0.8)
                .set(DomainElement.of(new int[]{2}), 0.6)
                .set(DomainElement.of(new int[]{3}), 0.4)
                .set(DomainElement.of(new int[]{4}), 0.2);
        Debug.print(set1, "Set1:");
        IDomain d2 = AbstractDomain.intRange(-5, 6);
        IFuzzySet set2 = new CalculatedFuzzySet(
                d2,
                StandardFuzzySets.lambdaFunction(
                        d2.indexOf(DomainElement.of(new int[]{-4})),
                        d2.indexOf(DomainElement.of(new int[]{0})),
                        d2.indexOf(DomainElement.of(new int[]{4}))
                )
        );
        Debug.print(set2, "Set2:");
    }

}
