package lab1.domain;

import java.util.Objects;

/**
 * Implementation of indexOf and elementForIndex method.
 */
public abstract class AbstractDomain implements IDomain {

    /**
     * @param from the first element
     * @param to right bound element, NOT INCLUDED
     * @return domain which represents a discrete interval: {from, from + 1, ... to-1}
     */
    public static IDomain intRange(int from, int to) {

        if (to > from) {
            return new SimpleDomain(from, to);

        } else {
            throw new IllegalArgumentException(
                    "Element 'to' must be greater than element 'from'!" +
                            "\nYou provided: from=" + from + ", to=" + to + "."
            );
        }

    }

    /**
     * Creates composition of domains using the two provided sub-domains.
     * @param firstDomain a first domain
     * @param secondDomain a second domain
     * @return cartesian product
     */
    public static AbstractDomain combine(IDomain firstDomain, IDomain secondDomain) {

        Objects.requireNonNull(firstDomain);
        Objects.requireNonNull(secondDomain);

        return new CompositeDomain(
                new SimpleDomain[]{
                    (SimpleDomain) Objects.requireNonNull(firstDomain),
                    (SimpleDomain) Objects.requireNonNull(secondDomain)
                }
        );

    }

    @Override
    public int indexOf(DomainElement element) {
        int currentIndex = 0;

        for (DomainElement current : this) {
            if (current.equals(element)) {
                return currentIndex;
            }

            ++currentIndex;
        }

        return -1;
    }

    @Override
    public DomainElement elementForIndex(int index) {
        int currentIndex = 0;

        for (DomainElement current : this) {
            if (currentIndex == index) {
                return current;
            }
            ++currentIndex;
        }

        return null;
    }

}
