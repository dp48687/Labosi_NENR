package lab1.domain;

import java.util.Iterator;
import java.util.Objects;

/**
 * Represents a domain which contains multiple sub-domains.
 * This is suitable for cartesian product.
 */
public class CompositeDomain extends AbstractDomain {

    /**
     * Domains stored in this composite domain.
     */
    private SimpleDomain[] domains;

    /**
     * Initializes domains.
     * @param simpleDomains domains which will form this composite domain
     */
    public CompositeDomain(SimpleDomain[] simpleDomains) {
        this.domains = Objects.requireNonNull(simpleDomains);
    }

    @Override
    public int getCardinality() {
        int cardinality = 1;

        for (SimpleDomain sd : domains) {
            cardinality *= sd.getCardinality();
        }

        return cardinality;
    }

    @Override
    public IDomain getComponent(int index) {
        return domains[index];
    }

    @Override
    public int getNumberOfComponents() {
        return domains.length;
    }

    @Override
    public Iterator<DomainElement> iterator() {
        return new Iterator<>() {
            private int[] indices = new int[getNumberOfComponents()];
            private int currentIndex;

            @Override
            public boolean hasNext() {
                return currentIndex < getCardinality();
            }

            @Override
            public DomainElement next() {
                if (hasNext()) {
                    int[] vector = new int[getNumberOfComponents()];
                    for (int i = 0; i < getNumberOfComponents(); ++i) {
                        vector[i] = getComponent(i).elementForIndex(indices[i]).getComponentValue(0);
                    }
                    increment();

                    return new DomainElement(vector);
                } else {
                    throw new IllegalStateException(
                        "This iterator has already reached its end!"
                    );
                }
            }

            private void increment() {
                ++currentIndex;

                int currentDomain = indices.length - 1;
                ++indices[currentDomain];
                while (currentDomain >= 0 && indices[currentDomain] >= getComponent(currentDomain).getCardinality()) {
                    indices[currentDomain] = 0;
                    if (currentDomain - 1 >= 0) {
                        ++indices[--currentDomain];
                    }
                }
            }
        };
    }

}
