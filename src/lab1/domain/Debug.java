package lab1.domain;

import lab1.fuzzy.IFuzzySet;

/**
 * Used to print domains as well as its elements.
 */
public class Debug {

    public static void print(IDomain domain, String headingText) {
        if (headingText != null) {
            System.out.println(headingText);
        }
        for(DomainElement e : domain) {
            System.out.println("Domain element: " + e);
        }
        System.out.println("Domain cardinality: " + domain.getCardinality());
        System.out.println();
    }

    public static void print(IFuzzySet set, String headingText) {
        if (headingText != null) {
            System.out.println(headingText);
        }
        System.out.println(set);
        System.out.println();
    }

}
