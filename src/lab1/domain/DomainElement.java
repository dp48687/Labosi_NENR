package lab1.domain;

import java.util.Arrays;
import java.util.Objects;

/**
 * Represents a domain element which can be indexed using a vector of
 * indices. For example, if we have a cartesian product of N domains,
 * then this element could be indexed by a vector of N components.
 */
public class DomainElement {

    /**
     * Indices used when we need to retrieve this domain element.
     */
    private int[] values;

    /**
     * Initializes values of this element.
     * @param values indices used to retrieve this element.
     */
    public DomainElement(int[] values) {
        this.values = Objects.requireNonNull(values);
    }

    /**
     * Number of sub domains in a domain where this element
     * could be member.
     * @return number of components of vector of indices
     */
    public int getNumberOfComponents() {
        return values.length;
    }

    /**
     * @return component value at <code>index</code>
     */
    public int getComponentValue(int index) {
        return values[index];
    }

    /**
     * Creates an element with specified indices.
     * @return created element
     */
    public static DomainElement of(int[] values) {
        return new DomainElement(values);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DomainElement that = (DomainElement) o;
        return Arrays.equals(values, that.values);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(values);
    }

    @Override
    public String toString() {
        return values.length > 1 ?
                Arrays.toString(values)
                        .replace("[", "(")
                        .replace("]", ")") :
                Integer.toString(values[0]);
    }

}
