package lab1.domain;

/**
 * An abstraction of domain.
 * This can be iterated over domain elements.
 */
public interface IDomain extends Iterable<DomainElement> {

    /**
     * @return number of elements in this domain
     */
    int getCardinality();

    /**
     * @return component indexed with index provided as an argument
     * @param index indexes the element which will be retrieved
     */
    IDomain getComponent(int index);

    /**
     * Number of sub-domains.
     * @return number of sub domains
     */
    int getNumberOfComponents();

    /**
     * @param element element which index will be returned, if it exists
     * @return index of the specific element if it exists, -1 otherwise
     */
    int indexOf(DomainElement element);

    /**
     * @param index index of searched element
     * @return the element indexed with <code>index</code>
     */
    DomainElement elementForIndex(int index);

}
