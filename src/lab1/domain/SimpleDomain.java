package lab1.domain;

import java.util.Iterator;

/**
 * Represents a concrete domain which is actually a discrete interval {from...to}, to
 * is not included.
 */
public class SimpleDomain extends AbstractDomain {

    /**
     * The first element of this simple domain (inclusive).
     */
    private int first;

    /**
     * The second element of this simple domain (exclusive).
     */
    private int second;

    /**
     * Initializes both attributes.
     *
     * @param first  a first element
     * @param second a second element
     */
    public SimpleDomain(int first, int second) {
        if (second < first) {
            throw new IllegalArgumentException(
                    "Second element is bigger than the first." +
                            "\nFirst: " + first + ", second: " + second + "."
            );
        } else {
            this.first = first;
            this.second = second;
        }
    }

    @Override
    public int getCardinality() {
        return second - first;
    }

    @Override
    public IDomain getComponent(int index) {
        if (index == 0) {
            return this;
        } else {
            throw new IndexOutOfBoundsException(
                    "Wrong index: " + index + ", allowed: 0."
            );
        }
    }

    @Override
    public int getNumberOfComponents() {
        return 1;
    }

    @Override
    public Iterator<DomainElement> iterator() {
        return new Iterator<>() {
            int currentElement = first;

            @Override
            public boolean hasNext() {
                return currentElement < second;
            }

            @Override
            public DomainElement next() {
                if (hasNext()) {
                    return new DomainElement(
                            new int[]{currentElement++}
                    );
                } else {
                    throw new IllegalStateException(
                                    "This iterator has already reached its last element." +
                                    "\nPossible elements: [" + first + ", " + second + ">."
                    );
                }
            }
        };
    }

    /**
     * A simple getter which retrieves the first component.
     * @return first component
     */
    public int getFirst() {
        return first;
    }

    /**
     * A simple getter which retrieves the second component.
     * @return second component
     */
    public int getSecond() {
        return second;
    }

}
