package lab1.fuzzy;

import lab1.domain.DomainElement;
import lab1.domain.IDomain;

import java.util.Iterator;
import java.util.Objects;

/**
 * A fuzzy set which has a domain and an unitary function which is called
 * only when user wants to retrieve a value at the specified index.
 */
public class CalculatedFuzzySet implements IFuzzySet {

    /**
     * Domain which stores indices.
     */
    private IDomain domain;

    /**
     * Function which will be called when user calls
     * <code>getDomainValueAt()</code> on this object.
     */
    private IIntUnitaryFunction unitaryFunction;

    /**
     * Initializes parameters.
     * @param domain a domain
     * @param unitaryFunction a function
     */
    public CalculatedFuzzySet(IDomain domain, IIntUnitaryFunction unitaryFunction) {
        this.domain = Objects.requireNonNull(domain);
        this.unitaryFunction = Objects.requireNonNull(unitaryFunction);
    }

    @Override
    public IDomain getDomain() {
        return domain;
    }

    @Override
    public double getDomainValueAt(DomainElement element) {
        return unitaryFunction.valueAt(domain.indexOf(element));
    }

    @Override
    public Iterator<Double> iterator() {
        throw new IllegalArgumentException(
                "Not implemented here!"
        );
    }

    @Override
    public String toString() {
        return representation();
    }

}
