package lab1.fuzzy;

import lab1.domain.DomainElement;
import lab1.domain.IDomain;

import java.text.DecimalFormat;

/**
 * Represents a fuzzy set which can evaluate values of the stored function
 * at a given domain element.
 */
public interface IFuzzySet extends Iterable<Double> {

    /**
     * Used to formatNumber function values to 6 decimal places.
     */
    DecimalFormat formatter = new DecimalFormat("#0.000000");

    /**
     * A simple getter.
     * @return domain
     */
    IDomain getDomain();

    /**
     * @param element represents index at which function will be called
     * @return a domain value
     */
    double getDomainValueAt(DomainElement element);

    /**
     * @return string representation of x -> mu(x) mapping
     */
    default String representation() {
        StringBuilder builder = new StringBuilder();

        String begin = "d(";
        String end = ") = ";

        for (DomainElement e : getDomain()) {
            builder.append(begin);
            builder.append(e.toString().replace("(", "").replace(")", ""));
            builder.append(end);
            builder.append(formatter.format(getDomainValueAt(e)));
            builder.append('\n');
        }
        builder.deleteCharAt(builder.toString().length() - 1);

        return builder.toString();
    }

}
