package lab1.fuzzy;

/**
 * A function which accepts only one parameter and thus can be calculated
 * only on simple domains.
 */
public interface IIntUnitaryFunction {

    /**
     * Calculates the function value at the specified index.
     * @param index function argument
     * @return function value for a given argument
     */
    double valueAt(int index);

}
