package lab1.fuzzy;

import lab1.domain.DomainElement;
import lab1.domain.IDomain;

import java.util.Iterator;
import java.util.Objects;

/**
 * Represents a fuzzy set whose values are set by user.
 */
public class MutableFuzzySet implements IFuzzySet {

    /**
     * Stores the values of this set which are calculated using some concrete function.
     */
    private double[] memberships;

    /**
     * A domain.
     */
    private IDomain domain;

    /**
     * Initializes domain and creates a field of values.
     * @param domain a domain
     */
    public MutableFuzzySet(IDomain domain) {
        this.domain = Objects.requireNonNull(domain);
        memberships = new double[domain.getCardinality()];
    }

    /**
     * Sets the given value to vector of values.
     * @param element an element
     * @param value a new value
     */
    public MutableFuzzySet set(DomainElement element, double value) {
        memberships[domain.indexOf(element)] = value;
        return this;
    }

    @Override
    public IDomain getDomain() {
        return domain;
    }

    @Override
    public double getDomainValueAt(DomainElement element) {
        return memberships[domain.indexOf(element)];
    }

    @Override
    public Iterator<Double> iterator() {
        return new Iterator<>() {
            private int currentIndex;

            @Override
            public boolean hasNext() {
                return currentIndex < domain.getCardinality();
            }

            @Override
            public Double next() {
                if (hasNext()) {
                    return memberships[currentIndex++];
                } else {
                    throw new IllegalStateException(
                            "This iterator has already reached its end."
                    );
                }
            }
        };
    }

    @Override
    public String toString() {
        return representation();
    }

}
