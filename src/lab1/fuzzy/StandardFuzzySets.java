package lab1.fuzzy;

/**
 * Stores concrete fuzzy sets (l, lambda and gamma function).
 */
public class StandardFuzzySets {

    public static IIntUnitaryFunction lFunction(int alpha, int beta) {
        return index -> {
            if (index < alpha) {
                return 1;
            } else if (index >= beta) {
                return 0;
            } else {
                return (beta - (double) index) / (beta - alpha);
            }
        };
    }

    public static IIntUnitaryFunction gammaFunction(int alpha, int beta) {
        return index -> {
            if (index < alpha) {
                return 0;
            } else if (index >= beta) {
                return 1;
            } else {
                return (index - (double) alpha) / (beta - alpha);
            }
        };
    }

    public static IIntUnitaryFunction lambdaFunction(int alpha, int beta, int gamma) {
        return index -> {
            if (index < alpha || index >= gamma) {
                return 0;
            } else if (alpha <= index && index < beta) {
                return (index - (double) alpha) / (beta - alpha);
            } else {
                return (gamma - (double) index) / (gamma - beta);
            }
        };
    }

    public static IIntUnitaryFunction gaussian(int alpha, int beta, double steepness) {
        return index -> {
            if (index < alpha || index > beta) {
                return 0;
            } else {
                double shift = (alpha + beta) / 2.0;
                double argSquared = steepness * (index - shift) * (index - shift);
                return Math.exp(-argSquared);
            }
        };
    }

    public static IIntUnitaryFunction leftGaussian(int alpha, int beta, double steepness) {
        return index -> {
            double shift = (alpha + beta) / 2.0;
            if (index > shift || index < alpha) {
                return 0;
            } else {
                double argSquared = steepness * (index - shift) * (index - shift);
                return Math.exp(-argSquared);
            }
        };
    }

    public static IIntUnitaryFunction rightGaussian(int alpha, int beta, double steepness) {
        return index -> {
            double shift = (alpha + beta) / 2.0;
            if (index < shift || index > beta) {
                return 0;
            } else {
                double argSquared = steepness * (index - shift) * (index - shift);
                return Math.exp(-argSquared);
            }
        };
    }

}
