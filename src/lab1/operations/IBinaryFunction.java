package lab1.operations;

/**
 * A binary function - represents mapping (x, y) -> f(x, y).
 */
public interface IBinaryFunction {

    /**
     * Calculates function value at given 2D point (x, y).
     * @param value1 first parameter
     * @param value2 second parameter
     * @return function value
     */
    double valueAt(double value1, double value2);
}
