package lab1.operations;

/**
 * An unary function, accepts one parameter.
 */
public interface IUnaryFunction {

    /**
     * @param value1 a parameter
     * @return function value
     */
    double valueAt(double value1);

}
