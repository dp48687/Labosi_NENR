package lab1.operations;

import lab1.domain.DomainElement;
import lab1.domain.IDomain;
import lab1.fuzzy.IFuzzySet;

import java.util.Iterator;

/**
 * Contains concrete operations.
 */
public class Operations {

    public static IFuzzySet unaryOperation(IFuzzySet set, IUnaryFunction function) {
        return new IFuzzySet() {
            @Override
            public IDomain getDomain() {
                return set.getDomain();
            }

            @Override
            public double getDomainValueAt(DomainElement element) {
                return function.valueAt(set.getDomainValueAt(element));
            }

            @Override
            public Iterator<Double> iterator() {
                throw new IllegalArgumentException(
                        "Not implemented here!"
                );
            }

            @Override
            public String toString() {
                return representation();
            }

        };
    }

    public static IFuzzySet binaryOperation(IFuzzySet set1, IFuzzySet set2, IBinaryFunction function) {

        return new IFuzzySet() {

            @Override
            public IDomain getDomain() {
                return set1.getDomain();
            }

            @Override
            public double getDomainValueAt(DomainElement element) {
                return function.valueAt(
                        set1.getDomainValueAt(element),
                        set2.getDomainValueAt(element)
                );
            }

            @Override
            public Iterator<Double> iterator() {
                throw new IllegalArgumentException(
                        "Not implemented here!"
                );
            }

            @Override
            public String toString() {
                return representation();
            }

        };
    }

    public static IUnaryFunction linear() {
        return value -> value;
    }

    public static IUnaryFunction sigmoid(double shift, double steepness) {
        return value -> 1.0 / (1.0 + Math.exp(steepness * (value - shift)));
    }

    public static IUnaryFunction gaussian(double mu, double sigmaSquared) {
        return value -> Math.exp(-(value - mu) * (value - mu) / (2 * sigmaSquared));
    }

    public static IUnaryFunction zadehNot() {
        return value -> 1.0 - value;
    }

    public static IBinaryFunction zadehAnd() {
        return Math::min;
    }

    public static IBinaryFunction zadehOr() {
        return Math::max;
    }

    public static IBinaryFunction hamacherTNorm(double value) {
        return (v1, v2) -> (v1 * v2) / (value + (1 - value) * (v1 + v2 - v1 * v2));
    }

    public static IBinaryFunction hamacherSNorm(double value) {
        return (v1, v2) -> (v1 + v2 - (2 - value) * (v1 * v2)) / (1 - (1 - value) * v1 * v2);
    }

}
