package lab2;

import lab1.domain.CompositeDomain;
import lab1.domain.DomainElement;
import lab1.domain.IDomain;
import lab1.domain.SimpleDomain;
import lab1.fuzzy.IFuzzySet;
import lab1.fuzzy.MutableFuzzySet;

/**
 * Contains methods for determining whether some relation is UxU, symmetric, reflexive,
 * min-max transitive or is fuzzy equivalence.
 */
public class Relations {

    /**
     * @param relation relation which will be inspected
     * @return is this relation U X U (subdomains of equal cardinality)
     */
    public static boolean isUtimesURelation(IFuzzySet relation) {
        if (relation.getDomain().getNumberOfComponents() == 2) {
            int cardinality = relation.getDomain().getCardinality();
            for (int i = 0; i < cardinality / 2; ++i) {
                if (i * i == cardinality) {
                    return true;
                } else if (i * i > cardinality) {
                    return false;
                }
            }
        }
        return false;
    }

    /**
     * @param relation relation which will be inspected
     * @return is this relation symmetric (mu(x,y) == mu(y,x), for each x, y)
     */
    public static boolean isSymmetric(IFuzzySet relation) {
        if (isUtimesURelation(relation)) {

            CompositeDomain cd = (CompositeDomain) (relation.getDomain());
            if (cd.getComponent(0).getCardinality() != cd.getComponent(1).getCardinality()) {
                return false;
            }

            SimpleDomain d1 = ((SimpleDomain) cd.getComponent(0));
            SimpleDomain d2 = ((SimpleDomain) cd.getComponent(1));

            for (int i = d1.getFirst(); i < d1.getFirst() + d1.getCardinality(); ++i) {
                for (int j = d2.getFirst(); j < d2.getFirst() + d2.getCardinality(); ++j) {
                    double value1 = relation.getDomainValueAt(DomainElement.of(new int[]{i, j}));
                    double value2 = relation.getDomainValueAt(DomainElement.of(new int[]{j, i}));

                    if (Double.compare(Math.abs(value1 - value2), 1E-9) == 1) {
                        return false;
                    }

                }
            }

            return true;

        } else {
            return false;
        }

    }

    /**
     * @param relation relation which will be inspected
     * @return is this relation reflexive (mu(x,x) == 0 for each x)
     */
    public static boolean isReflexive(IFuzzySet relation) {
        if (isUtimesURelation(relation)) {

            CompositeDomain cd = (CompositeDomain) (relation.getDomain());
            if (cd.getComponent(0).getCardinality() != cd.getComponent(1).getCardinality()) {
                return false;
            }

            SimpleDomain d1 = (SimpleDomain) (cd.getComponent(0));

            for (int i = d1.getFirst(); i < d1.getFirst() + d1.getCardinality(); ++i) {
                double value1 = relation.getDomainValueAt(DomainElement.of(new int[]{i, i}));
                if (Double.compare(Math.abs(value1 - 1), 1E-9) == 1) {
                    return false;
                }
            }

            return true;

        } else {
            return false;

        }

    }

    /**
     * @param relation relation which will be inspected
     * @return is relation min max transitive (mu(x, z) >= max{min[mu(x, y), mu(y, z)]} for each x, y, z)
     */
    public static boolean isMaxMinTransitive(IFuzzySet relation) {
        if (isUtimesURelation(relation)) {

            SimpleDomain d1 = (SimpleDomain) (relation.getDomain().getComponent(0));
            SimpleDomain d2 = (SimpleDomain) (relation.getDomain().getComponent(1));

            int startX = d1.getFirst();
            int startZ = d2.getFirst();
            int startY = d2.getFirst();

            for (int i = startX; i < startX + d1.getCardinality(); ++i) {
                for (int j = startZ; j < startZ + d2.getCardinality(); ++j) {
                    double xz = relation.getDomainValueAt(DomainElement.of(new int[]{i, j}));

                    double s = -1;
                    for (int k = startY; k < startY + d2.getCardinality(); ++k) {
                        double xy = relation.getDomainValueAt(DomainElement.of(new int[]{i, k}));
                        double yz = relation.getDomainValueAt(DomainElement.of(new int[]{k, j}));
                        s = Math.max(
                                Math.min(xy, yz),
                                s
                        );
                    }

                    if (xz < s) {
                        return false;
                    }
                }
            }

            return true;

        } else {

            return false;
        }
    }

    /**
     * @param r1 the first relation
     * @param r2 the second relation
     * @return composition of relations
     */
    public static IFuzzySet compositionOfBinaryRelations(IFuzzySet r1, IFuzzySet r2) {
        int numberOfComponents1 = r1.getDomain().getNumberOfComponents();
        int numberOfComponents2 = r2.getDomain().getNumberOfComponents();

        IDomain domain1 = r1.getDomain();
        IDomain domain2 = r2.getDomain();
        if (numberOfComponents1 == 2 && numberOfComponents2 == 2) {

            int u1 = domain1.getComponent(0).getCardinality();
            int v1 = domain1.getComponent(1).getCardinality();

            int u2 = domain2.getComponent(0).getCardinality();
            int v2 = domain2.getComponent(1).getCardinality();

            if (v1 != u2) {
                throw new IllegalArgumentException(
                        "Given relations are not compatible: " +
                                "(" + u1 + "," + v1 + ") x (" + u2 + "," + v2 + ")"
                );
            }

        } else {
            throw new IllegalArgumentException(
                    "Relations do not have the same number of components: " +
                            "\nr1 = V(" + numberOfComponents1 + ")" +
                            "\nr2 = V(" + numberOfComponents2 + ")"
            );
        }

        CompositeDomain cd = new CompositeDomain(
                new SimpleDomain[]{
                        (SimpleDomain) (domain1.getComponent(0)),
                        (SimpleDomain) (domain2.getComponent(1))
                }
        );

        MutableFuzzySet created = new MutableFuzzySet(cd);

        int x = domain1.getComponent(0).getCardinality();
        int startX = ((SimpleDomain) (domain1.getComponent(0))).getFirst();
        int y = domain1.getComponent(1).getCardinality();
        int startY = ((SimpleDomain) (domain1.getComponent(1))).getFirst();
        int z = domain2.getComponent(1).getCardinality();
        int startZ = ((SimpleDomain) (domain2.getComponent(1))).getFirst();

        for (int i = startX; i < startX + x; ++i) {
            for (int j = startZ; j < startZ + z; ++j) {
                double max = -1;
                for (int k = startY; k < y + startY; ++k) {
                    max = Math.max(
                            Math.min(
                                    r1.getDomainValueAt(DomainElement.of(new int[]{i, k})),
                                    r2.getDomainValueAt(DomainElement.of(new int[]{k, j}))
                            ),
                            max
                    );
                }
                created.set(DomainElement.of(new int[]{i, j}), max);
            }
        }

        return created;
    }

    /**
     * @param fuzzySet will be inspected
     * @return is symmetric, reflexive and max-min transitive
     */
    public static boolean isFuzzyEquivalence(IFuzzySet fuzzySet) {
        return isSymmetric(fuzzySet) && isReflexive(fuzzySet) && isMaxMinTransitive(fuzzySet);
    }

}