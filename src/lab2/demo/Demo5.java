package lab2.demo;

import lab1.domain.AbstractDomain;
import lab1.domain.DomainElement;
import lab1.domain.IDomain;
import lab1.fuzzy.IFuzzySet;
import lab1.fuzzy.MutableFuzzySet;
import lab2.Relations;

public class Demo5 {

    public static void main(String[] args) {
        IDomain u1 = AbstractDomain.intRange(1, 5); // {1,2,3,4}
        IDomain u2 = AbstractDomain.intRange(1, 4); // {1,2,3}
        IDomain u3 = AbstractDomain.intRange(1, 5); // {1,2,3,4}

        IFuzzySet r1 = new MutableFuzzySet(AbstractDomain.combine(u1, u2))
                .set(DomainElement.of(new int[]{1, 1}), 0.3)
                .set(DomainElement.of(new int[]{1, 2}), 1)
                .set(DomainElement.of(new int[]{3, 3}), 0.5)
                .set(DomainElement.of(new int[]{4, 3}), 0.5);

        IFuzzySet r2 = new MutableFuzzySet(AbstractDomain.combine(u2, u3))
                .set(DomainElement.of(new int[]{1, 1}), 1)
                .set(DomainElement.of(new int[]{2, 1}), 0.5)
                .set(DomainElement.of(new int[]{2, 2}), 0.7)
                .set(DomainElement.of(new int[]{3, 3}), 1)
                .set(DomainElement.of(new int[]{3, 4}), 0.4);

        IFuzzySet r1r2 = Relations.compositionOfBinaryRelations(r1, r2);

        for(DomainElement e : r1r2.getDomain()) {
            System.out.println("mu(" + e + ")=" + r1r2.getDomainValueAt(e));
        }

    }

}
