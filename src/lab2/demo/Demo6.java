package lab2.demo;

import lab1.domain.AbstractDomain;
import lab1.domain.DomainElement;
import lab1.domain.IDomain;
import lab1.fuzzy.IFuzzySet;
import lab1.fuzzy.MutableFuzzySet;
import lab2.Relations;

public class Demo6 {

    public static void main(String[] args) {
        IDomain u = AbstractDomain.intRange(1, 5);
        IFuzzySet r = new MutableFuzzySet(AbstractDomain.combine(u, u))
                .set(DomainElement.of(new int[]{1, 1}), 1)
                .set(DomainElement.of(new int[]{2, 2}), 1)
                .set(DomainElement.of(new int[]{3, 3}), 1)
                .set(DomainElement.of(new int[]{4, 4}), 1)
                .set(DomainElement.of(new int[]{1, 2}), 0.3)
                .set(DomainElement.of(new int[]{2, 1}), 0.3)
                .set(DomainElement.of(new int[]{2, 3}), 0.5)
                .set(DomainElement.of(new int[]{3, 2}), 0.5)
                .set(DomainElement.of(new int[]{3, 4}), 0.2)
                .set(DomainElement.of(new int[]{4, 3}), 0.2);

        IFuzzySet r2 = r;
        System.out.println("Početna relacija je neizrazita relacija ekvivalencije? " + Relations.isFuzzyEquivalence(r2));
        System.out.println();

        for (int i = 1; i <= 3; ++i) {
            r2 = Relations.compositionOfBinaryRelations(r2, r);
            System.out.println("Broj odrađenih kompozicija: " + i + ". Relacija je:");

            for (DomainElement e : r2.getDomain()) {
                System.out.println("mu(" + e + ")=" + r2.getDomainValueAt(e));
            }

            System.out.println("Ova relacija je neizrazita relacija ekvivalencije? " + Relations.isFuzzyEquivalence(r2));
            System.out.println();
        }

    }

}
