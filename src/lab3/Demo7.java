package lab3;

import lab3.defuzzification.COADefuzzifier;
import lab3.defuzzification.IDefuzzifier;
import lab3.fuzzysystem.AbstractFuzzySystem;
import lab3.fuzzysystem.concrete.AcceleratorFuzzySystem;
import lab3.fuzzysystem.concrete.SteeringFuzzySystem;
import lab3.rules.Rule;

import java.util.Scanner;

public class Demo7 {

    public static void main(String[] args) {

        launchBoat();

        // concludeForAcceleration(
        //         new Rule(Arrays.asList(NEAR_SHORE, null, null, null, FAST_SPEED, null), FuzzySets.STOP_INCREASING_SPEED),
        //         new int[]{40, 40, 20, 76, 90, 0}
        // );

        // concludeForAcceleration(new int[]{40, 40, 20, 76, 90, 0});

    }

    /**
     *
     */
    private static void launchBoat() {
        Scanner scanner = new Scanner(System.in);

        IDefuzzifier defuzzifier = new COADefuzzifier();

        AbstractFuzzySystem steeringMachine = new SteeringFuzzySystem(defuzzifier);
        AbstractFuzzySystem acceleratorMachine = new AcceleratorFuzzySystem(defuzzifier);

        while (true) {
            int observedValues[] = new int[6];
            for (int i = 0; i < observedValues.length; ++i) {
                observedValues[i] = scanner.nextInt();
            }

            // Defuzzified value is in interval [0, 180], so we have to subtract 90 degrees.
            double K = steeringMachine.process(observedValues) - 90;
            double A = acceleratorMachine.process(observedValues);

            System.out.printf("%d %d\n", (int) A, (int) K);
            System.out.flush();
        }
    }

    /**
     * Concludes the output for the single rule for acceleration.
     *
     * @param rule
     * @param input
     */
    private static void concludeForAcceleration(Rule rule, int[] input) {
        AcceleratorFuzzySystem as = new AcceleratorFuzzySystem(new COADefuzzifier());
        as.process(rule, input);
    }

    /**
     * Concludes the output for all rules for acceleration.
     *
     * @param input
     */
    private static void concludeForAcceleration(int[] input) {
        AcceleratorFuzzySystem as = new AcceleratorFuzzySystem(new COADefuzzifier());
        double concluded = as.process(as.acceleratorRules, input);
    }

}
