package lab3.defuzzification;

import lab1.domain.DomainElement;
import lab1.fuzzy.IFuzzySet;

public class COADefuzzifier implements IDefuzzifier {

    @Override
    public double defuzzify(IFuzzySet set) {
        double a = 0, b = 0;

        for (DomainElement e : set.getDomain()) {
            a += e.getComponentValue(0) * set.getDomainValueAt(e);
            b += set.getDomainValueAt(e);
        }

        double result = a / b;

        return Double.isNaN(result) ? 0 : result;
    }

}
