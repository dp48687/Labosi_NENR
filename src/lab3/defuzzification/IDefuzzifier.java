package lab3.defuzzification;

import lab1.fuzzy.IFuzzySet;

/**
 *
 * @author Danijel
 */
public interface IDefuzzifier {

    /**
     *
     * @param set
     * @return
     */
    double defuzzify(IFuzzySet set);

}
