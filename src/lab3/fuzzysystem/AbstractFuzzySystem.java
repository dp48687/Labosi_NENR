package lab3.fuzzysystem;

import lab1.fuzzy.IFuzzySet;
import lab1.operations.IBinaryFunction;
import lab1.operations.Operations;
import lab3.defuzzification.IDefuzzifier;
import lab3.rules.Rule;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Danijel
 */
public abstract class AbstractFuzzySystem {

    /**
     *
     */
    public static ConclusionType type = ConclusionType.MINIMUM;

    /**
     *
     */
    protected IDefuzzifier defuzzifier;

    /**
     *
     * @param defuzzifier
     */
    public AbstractFuzzySystem(IDefuzzifier defuzzifier) {
        this.defuzzifier = defuzzifier;
    }

    /**
     *
     * @param values
     * @return
     */
    public double process(int[] values) {

        List<IFuzzySet> consequents = new ArrayList<>();
        for (Rule r : getRules()) {
            consequents.add(r.process(values));
        }

        IFuzzySet result = consequents.get(0);
        IBinaryFunction union = Operations.zadehOr();

        for (int i = 1; i < consequents.size(); ++i) {
            result = Operations.binaryOperation(result, consequents.get(i), union);
        }

        return defuzzifier.defuzzify(result);

    }

    /**
     *
     * @param values
     * @return
     */
    public double process(Rule rule, int[] values) {
        IFuzzySet result = rule.process(values);
        double value = defuzzifier.defuzzify(result);

        System.out.println("VALUES: " + Arrays.toString(values));
        System.out.println("CONSEQUENT: " + result);
        System.out.println("DEFUZZIFIED VALUE: " + value);

        return value;
    }

    /**
     * @param values
     * @return
     */
    public double process(Rule[] rule, int[] values) {

        List<IFuzzySet> consequents = new ArrayList<>();
        for (Rule r : getRules()) {
            consequents.add(r.process(values));
        }

        IFuzzySet result = consequents.get(0);
        IBinaryFunction union = Operations.zadehOr();

        for (int i = 1; i < consequents.size(); ++i) {
            result = Operations.binaryOperation(result, consequents.get(i), union);
        }

        double value = defuzzifier.defuzzify(result);

        System.out.println("VALUES: " + Arrays.toString(values));
        System.out.println("CONSEQUENT: " + result);
        System.out.println("DEFUZZIFIED VALUE: " + value);

        return value;
    }

    protected abstract Rule[] getRules();

}