package lab3.fuzzysystem.concrete;

import lab3.defuzzification.IDefuzzifier;
import lab3.fuzzysystem.AbstractFuzzySystem;
import lab3.rules.FuzzySets;
import lab3.rules.Rule;

import java.util.Arrays;

import static lab3.rules.FuzzySets.*;

/**
 *
 * @author Danijel
 */
public class AcceleratorFuzzySystem extends AbstractFuzzySystem {

    public Rule[] acceleratorRules = new Rule[]{
            new Rule(Arrays.asList(NEAR_SHORE, null, null, null, FAST_SPEED, null), FuzzySets.STOP_INCREASING_SPEED),
            new Rule(Arrays.asList(null, NEAR_SHORE, null, null, FAST_SPEED, null), FuzzySets.STOP_INCREASING_SPEED),
            new Rule(Arrays.asList(null, null, NEAR_SHORE, null, FAST_SPEED, null), FuzzySets.STOP_INCREASING_SPEED),
            new Rule(Arrays.asList(null, null, null, NEAR_SHORE, FAST_SPEED, null), FuzzySets.STOP_INCREASING_SPEED),
            new Rule(Arrays.asList(null, null, FAR_AWAY, FAR_AWAY, SLOW_SPEED, null), FuzzySets.INCREASE_SPEED),
    };

    /**
     *
     * @param defuzzifier
     */
    public AcceleratorFuzzySystem(IDefuzzifier defuzzifier) {
        super(defuzzifier);
    }

    @Override
    protected Rule[] getRules() {
        return acceleratorRules;
    }

}
