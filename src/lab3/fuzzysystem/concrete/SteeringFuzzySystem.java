package lab3.fuzzysystem.concrete;

import lab3.defuzzification.IDefuzzifier;
import lab3.fuzzysystem.AbstractFuzzySystem;
import lab3.rules.Rule;

import java.util.Arrays;

import static lab3.rules.FuzzySets.*;

/**
 *
 * @author Danijel
 */
public class SteeringFuzzySystem extends AbstractFuzzySystem {

    /**
     *
     */
    public Rule[] rudderRules = new Rule[]{
            new Rule(Arrays.asList(null, FAR_AWAY, NEAR_SHORE, null, FAST_SPEED, null), TURN_HARD_RIGHT),
            new Rule(Arrays.asList(FAR_AWAY, null, null, NEAR_SHORE, FAST_SPEED, null), TURN_HARD_LEFT),
            new Rule(Arrays.asList(null, null, FAR_AWAY, NEAR_SHORE, null, WRONG_DIRECTION), TURN_HARD_LEFT),
            new Rule(Arrays.asList(null, null, NEAR_SHORE, FAR_AWAY, null, WRONG_DIRECTION), TURN_HARD_RIGHT),
            new Rule(Arrays.asList(NEAR_SHORE, null, NEAR_SHORE, NEAR_SHORE, WRONG_DIRECTION, WRONG_DIRECTION), TURN_SLIGHTLY_RIGHT),
            new Rule(Arrays.asList(null, NEAR_SHORE, NEAR_SHORE, NEAR_SHORE, WRONG_DIRECTION, WRONG_DIRECTION), TURN_SLIGHTLY_LEFT),
            new Rule(Arrays.asList(null, null, FAR_AWAY, null, null, WRONG_DIRECTION), TURN_HARD_LEFT),
            new Rule(Arrays.asList(null, null, null, FAR_AWAY, null, WRONG_DIRECTION), TURN_HARD_RIGHT),
    };


    /**
     *
     * @param defuzzifier
     */
    public SteeringFuzzySystem(IDefuzzifier defuzzifier) {
        super(defuzzifier);
    }

    @Override
    protected Rule[] getRules() {
        return rudderRules;
    }

}
