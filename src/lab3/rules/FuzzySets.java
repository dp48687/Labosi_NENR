package lab3.rules;

import lab1.domain.SimpleDomain;
import lab1.fuzzy.CalculatedFuzzySet;
import lab1.fuzzy.IFuzzySet;
import lab1.fuzzy.StandardFuzzySets;

/**
 *
 * @author Danijel
 */
public class FuzzySets {

    /**
     * Domains for fuzzy variables.
     */
    public static final SimpleDomain ANGLE = new SimpleDomain(0, 180);
    public static final SimpleDomain DISTANCE = new SimpleDomain(0, 1301);
    public static final SimpleDomain ACCELERATION = new SimpleDomain(-50, 75);
    public static final SimpleDomain VELOCITY = new SimpleDomain(0, 101);
    public static final SimpleDomain DIRECTION = new SimpleDomain(0, 1);


    public static final IFuzzySet WRONG_DIRECTION = new CalculatedFuzzySet(DIRECTION, StandardFuzzySets.lFunction(0, 1));

    public static final IFuzzySet SLOW_SPEED = new CalculatedFuzzySet(VELOCITY, StandardFuzzySets.lFunction(20, 50));
    public static final IFuzzySet FAST_SPEED = new CalculatedFuzzySet(VELOCITY, StandardFuzzySets.gammaFunction(20, 50));

    public static final IFuzzySet VERY_NEAR_SHORE = new CalculatedFuzzySet(DISTANCE, StandardFuzzySets.lFunction(0, 20));
    public static final IFuzzySet NEAR_SHORE = new CalculatedFuzzySet(DISTANCE, StandardFuzzySets.lambdaFunction(30, 55, 80));
    public static final IFuzzySet FAR_AWAY = new CalculatedFuzzySet(DISTANCE, StandardFuzzySets.gammaFunction(50, 80));

    public static final IFuzzySet INCREASE_SPEED = new CalculatedFuzzySet(ACCELERATION, StandardFuzzySets.gammaFunction(30, 60));
    public static final IFuzzySet STOP_INCREASING_SPEED = new CalculatedFuzzySet(ACCELERATION, StandardFuzzySets.lFunction(0, 30));

    public static final IFuzzySet TURN_SLIGHTLY_LEFT = new CalculatedFuzzySet(ANGLE, StandardFuzzySets.lambdaFunction(90, 110, 135));
    public static final IFuzzySet TURN_HARD_LEFT = new CalculatedFuzzySet(ANGLE, StandardFuzzySets.gammaFunction(90, 180));
    public static final IFuzzySet TURN_SLIGHTLY_RIGHT = new CalculatedFuzzySet(ANGLE, StandardFuzzySets.lambdaFunction(45, 65, 90));
    public static final IFuzzySet TURN_HARD_RIGHT = new CalculatedFuzzySet(ANGLE, StandardFuzzySets.lFunction(0, 90));

}
