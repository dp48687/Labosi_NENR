package lab3.rules;

import lab1.domain.DomainElement;
import lab1.domain.IDomain;
import lab1.fuzzy.IFuzzySet;
import lab3.fuzzysystem.AbstractFuzzySystem;
import lab3.fuzzysystem.ConclusionType;

import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Danijel
 */
public class Rule {

    /**
     *
     */
    private List<IFuzzySet> antecedent;

    /**
     *
     */
    private IFuzzySet consequent;

    /**
     *
     */
    private static int[] domainIndex = new int[1];

    /**
     *
     * @param antecedent
     * @param consequent
     */
    public Rule(List<IFuzzySet> antecedent, IFuzzySet consequent) {
        super();
        this.antecedent = antecedent;
        this.consequent = consequent;
    }

    /**
     *
     * @param values
     * @return
     */
    public IFuzzySet process(int[] values) {
        double[] mu = new double[]{1.0};

        int i = 0;
        for (IFuzzySet set : antecedent) {
            if (set != null) {
                domainIndex[0] = values[i];
                double current = set.getDomainValueAt(DomainElement.of(domainIndex));
                mu[0] = AbstractFuzzySystem.type == ConclusionType.MINIMUM ?
                        Math.min(mu[0], current) :
                        mu[0] * current;
            }

            ++i;
        }

        return new IFuzzySet() {

            @Override
            public IDomain getDomain() {
                return consequent.getDomain();
            }

            @Override
            public double getDomainValueAt(DomainElement element) {
                return Math.min(consequent.getDomainValueAt(element), mu[0]);
            }

            @Override
            public Iterator<Double> iterator() {
                return consequent.iterator();
            }

            @Override
            public String toString() {
                return representation();
            }

        };

    }

}
