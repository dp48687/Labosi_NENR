package lab4;

import lab4.fitnessfunction.IFunction;
import lab4.geneticalgorithm.GeneticAlgorithm;
import utils.various.FileIOUtils;

import java.util.Arrays;

import static java.lang.Math.*;

public class Demo8 {

    private static final int NUMBER_OF_EPOCHS = 1000000;

    private static double[][] dataset1 = readDataset("src\\lab4\\data\\dataset1.txt");
    private static double[][] dataset2 = readDataset("src\\lab4\\data\\dataset2.txt");

    private static IFunction fitnessFunctionD1 = makeFitnessFunction(dataset1);
    private static IFunction fitnessFunctionD2 = makeFitnessFunction(dataset2);


    public static void main(String[] args) {
        run(fitnessFunctionD1);
    }

    /**
     *
     * @param fitnessFunction
     */
    private static void run(IFunction fitnessFunction) {
        GeneticAlgorithm decimalGA = GeneticAlgorithm.fromString(
                FileIOUtils.readFileContents("src\\lab4\\configuration\\configuration1.txt")
        );
        decimalGA.attachFitnessFunction(fitnessFunction);

        for (int i = 0; i < NUMBER_OF_EPOCHS; ++i) {
            decimalGA.runGenerationVariation();
            if ((i + 1) % 1000 == 0) {
                System.out.println(
                        "EPOCH " + (i + 1) + ":    " +
                                "THE BEST MSE: " + fitnessFunction.calcuateFitness(decimalGA.getTheBestVariables()) +
                                "    VARIABLES: " + Arrays.toString(decimalGA.getTheBestVariables())
                );
            }
        }
    }

    /**
     *
     * @param dataset
     * @return
     */
    private static IFunction makeFitnessFunction(double[][] dataset) {
        return vars -> {
            double mse = 0.0;
            for (int i = 0; i < dataset.length; ++i) {
                double calculated = calculateUserFunction(dataset[i][0], dataset[i][1], vars);
                mse += (calculated - dataset[i][2]) * (calculated - dataset[i][2]);
            }
            return mse / dataset.length;
        };
    }

    /**
     *
     * @param x
     * @param y
     * @param variables
     * @return
     */
    public static double calculateUserFunction(double x, double y, double[] variables) {
        return sin(variables[0] + variables[1] * x) +
                variables[2] * cos(x * (variables[3] + y)) *
                        1.0 / (1 + exp((x - variables[4]) * (x - variables[4])));
    }

    /**
     *
     * @param path
     * @return
     */
    private static double[][] readDataset(String path) {
        String content = FileIOUtils.readFileContents(path);

        String[] rows = content.split("\n");
        int numberOfColumns = rows[0].split("\t").length;

        double[][] dataset = new double[rows.length][numberOfColumns];
        for (int i = 0; i < rows.length; ++i) {
            String[] rowData = rows[i].split("\t");
            for (int j = 0; j < numberOfColumns; ++j) {
                dataset[i][j] = Double.parseDouble(rowData[j]);
            }
        }

        return dataset;
    }

}
