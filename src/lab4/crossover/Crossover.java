package lab4.crossover;

import lab4.geneticalgorithm.Chromosome;

/**
 *
 */
public interface Crossover {

    /**
     *
     * @param parents
     * @param children
     */
    void crossover(Chromosome[] parents, Chromosome[] children);

}
