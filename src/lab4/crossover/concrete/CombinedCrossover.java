package lab4.crossover.concrete;

import lab4.crossover.Crossover;
import lab4.geneticalgorithm.Chromosome;
import utils.random.RandomNumberGenerator;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Danijel Pavlek
 */
public class CombinedCrossover implements Crossover {

    private int pointsToInterpolate;

    public CombinedCrossover() {
        this(1);
    }

    public CombinedCrossover(int pointsToInterpolate) {
        this.pointsToInterpolate = pointsToInterpolate;
    }

    @Override
    public void crossover(Chromosome[] parents, Chromosome[] children) {

        if (children.length > 1) {
            throw new IllegalArgumentException(
                    "This crossover operator can generate only one child!"
            );
        }

        Set<Integer> indices = new HashSet<>();

        int upperBoundCounter = 15;
        while (upperBoundCounter > 0 && indices.size() < pointsToInterpolate) {
            --upperBoundCounter;
            indices.add(
                    RandomNumberGenerator.nextIntIn(
                            0,
                            parents[0].getBody().length - 1
                    )
            );
        }

        for (int i = 0; i < parents[0].getBody().length; ++i) {
            if (indices.contains(i)) {
                children[0].setGeneAt(
                        i,
                        (parents[0].getGeneAt(i) + parents[1].getGeneAt(i)) / 2.0
                );
            } else {
                children[0].setGeneAt(
                        i,
                        RandomNumberGenerator.random.nextBoolean() ?
                                parents[0].getGeneAt(i) :
                                parents[1].getGeneAt(i)
                );
            }
        }

    }

}
