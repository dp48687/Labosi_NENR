package lab4.crossover.concrete;

import lab4.geneticalgorithm.Chromosome;
import lab4.crossover.Crossover;
import utils.random.RandomNumberGenerator;

/**
 *
 * @author Danijel Pavlek
 */
public class InterpolationCrossover implements Crossover {

    /**
     * Interpolation factor, must be in [0, 1].
     */
    protected double lambda;

    /**
     *
     */
    public InterpolationCrossover() {
        this(0.5);
    }

    /**
     *
     * @param lambda
     */
    public InterpolationCrossover(double lambda) {
        if (lambda < 0 || lambda > 1) {
            throw new IllegalArgumentException(
                    "Lambda must be in interval [0, 1]. You provided: " + lambda
            );
        } else {
            this.lambda = lambda;
        }
    }

    @Override
    public void crossover(Chromosome[] parents, Chromosome[] children) {
        int size = Chromosome.lowerBounds.length;
        int breakPoint = RandomNumberGenerator.nextIntIn(0, size - 1);

        for (int i = 0; i < size; ++i) {
            children[0].setGeneAt(
                    i, i < breakPoint ?
                            parents[0].getGeneAt(i) * lambda + parents[1].getGeneAt(i) * (1 - lambda) :
                            parents[1].getGeneAt(i) * lambda + parents[0].getGeneAt(i) * (1 - lambda)
            );
            if (children.length > 1) {
                children[1].setGeneAt(
                        i, i < breakPoint ?
                                parents[1].getGeneAt(i) * lambda + parents[0].getGeneAt(i) * (1 - lambda) :
                                parents[0].getGeneAt(i) * lambda + parents[1].getGeneAt(i) * (1 - lambda)
                );
            }
        }
    }

}
