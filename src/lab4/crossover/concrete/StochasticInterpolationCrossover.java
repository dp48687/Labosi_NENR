package lab4.crossover.concrete;

import lab4.geneticalgorithm.Chromosome;
import utils.random.RandomNumberGenerator;

/**
 *
 * @author Danijel Pavlek
 */
public class StochasticInterpolationCrossover extends InterpolationCrossover {

    public StochasticInterpolationCrossover() {

    }

    public StochasticInterpolationCrossover(double lambda) {
        super(lambda);
    }

    @Override
    public void crossover(Chromosome[] parents, Chromosome[] children) {
        lambda = RandomNumberGenerator.nextDoubleIn(0.05, 0.995);
        super.crossover(parents, children);
    }

}
