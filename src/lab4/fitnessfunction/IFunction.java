package lab4.fitnessfunction;

/**
 *
 * @author Danijel Pavlek
 */
public interface IFunction {

    /**
     *
     * @param variables
     * @return
     */
    double calcuateFitness(double[] variables);

}
