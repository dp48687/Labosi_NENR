package lab4.geneticalgorithm;

import utils.random.RandomNumberGenerator;

import java.util.Arrays;

/**
 *
 * @author Danijel Pavlek
 */
public class Chromosome implements Comparable<Chromosome> {

    /**
     *
     */
    public static double[] lowerBounds;

    /**
     *
     */
    public static double[] upperBounds;

    /**
     *
     */
    private double[] body;

    /**
     *
     */
    private double fitness = Double.MAX_VALUE;

    /**
     *
     */
    public Chromosome() {
        body = new double[lowerBounds.length];
        randomUniformInit();
    }

    /**
     *
     * @param genes
     */
    private Chromosome(double[] genes) {
        body = new double[genes.length];
        System.arraycopy(genes, 0, body, 0, genes.length);
    }

    /**
     *
     * @return
     */
    public Chromosome deepCopy() {
        Chromosome deepClone = new Chromosome(body);
        deepClone.fitness = fitness;

        return deepClone;
    }

    /**
     *
     */
    private void randomUniformInit() {
        for (int i = 0; i < body.length; ++i) {
            body[i] = RandomNumberGenerator.nextDoubleIn(lowerBounds[i], upperBounds[i]);
        }
    }

    /**
     *
     */
    private void randomGaussianInit() {
        for (int i = 0; i < body.length; ++i) {
            body[i] = RandomNumberGenerator.nextGaussian() * 50;
        }
    }

    /**
     *
     * @param index
     * @param value
     * @return
     */
    public boolean setGeneAt(int index, double value) {
        if (value < lowerBounds[index] || value > upperBounds[index]) {
            // body[index] = RandomNumberGenerator.nextDoubleIn(lowerBounds[index], upperBounds[index]);
            body[index] = value;
            return false;
        } else {
            body[index] = value;
            return true;
        }
    }

    /**
     *
     * @param index
     * @return
     */
    public double getGeneAt(int index) {
        return body[index];
    }

    /**
     * Returns chromosome genes
     * @return genes
     */
    public double[] getBody() {
        return body;
    }

    /**
     *
     * @return
     */
    public double getFitness() {
        return fitness;
    }

    /**
     *
     * @param fitness
     */
    public void setFitness(double fitness) {
        this.fitness = fitness;
    }

    @Override
    public int compareTo(Chromosome o) {
        return Double.compare(fitness, o.fitness);
    }

    @Override
    public String toString() {
        return Arrays.toString(body) + " -> " + fitness;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Chromosome that = (Chromosome) o;
        return Arrays.equals(body, that.body);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(body);
    }

}
