package lab4.geneticalgorithm;

import lab4.crossover.Crossover;
import lab4.fitnessfunction.IFunction;
import lab4.mutation.Mutation;
import utils.random.RandomNumberGenerator;

import java.util.*;

/**
 * An implementation of simple genetic algorithm.
 *
 * @author Danijel Pavlek
 */
public class GeneticAlgorithm {

    /**
     *
     */
    public static double survivorsRatio = 0.5;

    /**
     *
     */
    private boolean elitismEnabled;

    /**
     *
     */
    private List<Chromosome> population;

    /**
     *
     */
    private Crossover crossoverOperator;

    /**
     *
     */
    private Mutation mutationOperator;

    /**
     *
     */
    private IFunction fitnessFunction;

    /**
     *
     */
    private Chromosome theBest;

    /**
     * Fields which help to make a program more efficient.
     */
    private static Chromosome[] fieldToStoreParents = new Chromosome[2];
    private static Chromosome[] fieldToStoreChildren = new Chromosome[1];
    private static List<Integer> indices;

    /**
     *
     * @param populationSize
     * @param elitismEnabled
     * @param crossoverOperator
     * @param mutationOperator
     */
    GeneticAlgorithm(int populationSize,
                     boolean elitismEnabled,
                     Crossover crossoverOperator,
                     Mutation mutationOperator) {
        this.elitismEnabled = elitismEnabled;
        this.crossoverOperator = Objects.requireNonNull(
                crossoverOperator, "Crossover operator must not bee null!"
        );
        this.mutationOperator = Objects.requireNonNull(
                mutationOperator, "Mutation operator must not be null!"
        );
        createPopulation(populationSize);
    }

    /**
     *
     */
    private void createPopulation(int populationSize) {
        population = new ArrayList<>(populationSize);

        for (int i = 0; i < populationSize; ++i) {
            population.add(new Chromosome());
        }
    }

    /**
     *
     * @param configuration
     * @return
     */
    public static GeneticAlgorithm fromString(String configuration) {
        return new Parser(configuration).parse();
    }

    /**
     *
     * @return the best variables
     */
    public double[] processEliminationVariation() {
        if (fitnessFunction == null) {
            throw new IllegalStateException(
                    "The fitness function has not been initialized!"
            );
        }

        population.sort(Comparator.naturalOrder());

        kTournamentSelection(3);
        if (elitismEnabled) {
            elitisticSave();
        }

        return getTheBestVariables();
    }

    /**
     *
     * @return
     */
    public double[] runGenerationVariation() {

        int amountToSelect = (int) (population.size() * survivorsRatio);
        for (int i = amountToSelect; i < population.size(); ++i) {
            Chromosome parent1 = selectRandomIndividual(0, amountToSelect - 1);
            Chromosome parent2 = selectRandomIndividual(0, amountToSelect - 1);

            while (parent1 == parent2) {
                parent2 = selectRandomIndividual(0, amountToSelect - 1);
            }

            fieldToStoreParents[0] = parent1;
            fieldToStoreParents[1] = parent2;
            fieldToStoreChildren[0] = population.get(i);

            crossoverOperator.crossover(fieldToStoreParents, fieldToStoreChildren);
            mutationOperator.mutate(fieldToStoreChildren[0]);
            fieldToStoreChildren[0].setFitness(
                    fitnessFunction.calcuateFitness(fieldToStoreChildren[0].getBody())
            );

        }

        if (elitismEnabled) {
            elitisticSave();
        }
        population.sort(Comparator.naturalOrder());

        return getTheBestVariables();
    }

    /**
     *
     * @param k number of randomly selected individuals
     */
    private void kTournamentSelection(int k) {

        if (indices == null) {
            indices = new ArrayList<>(k);
        }

        while (indices.size() < k) {
            int index = RandomNumberGenerator.nextIntIn(0, population.size() - 1);
            if (!indices.contains(index)) {
                indices.add(index);
            }
        }

        indices.sort(Comparator.naturalOrder());
        fieldToStoreParents[0] = population.get(indices.get(0));
        fieldToStoreParents[1] = population.get(indices.get(1));

        for (int i = 2; i < k; ++i) {
            fieldToStoreChildren[0] = population.get(indices.get(i));
            crossoverOperator.crossover(fieldToStoreParents, fieldToStoreChildren);
            mutationOperator.mutate(fieldToStoreChildren[0]);
            fieldToStoreChildren[0].setFitness(
                    fitnessFunction.calcuateFitness(fieldToStoreChildren[0].getBody())
            );
        }

        indices.clear();
    }

    /**
     *
     * @return
     */
    public double[] getTheBestVariables() {
        return theBest == null ? population.get(0).getBody() : theBest.getBody();
    }

    /**
     *
     * @return
     */
    public double getTheBestFitness() {
        if (theBest != null) {
            return theBest.getFitness();
        } else {
            double theBest = population.get(0).getFitness();
            for (int i = 1; i < population.size(); ++i) {
                if (theBest > population.get(i).getFitness()) {
                    theBest = population.get(i).getFitness();
                }
            }
            return theBest;
        }
    }

    /**
     *
     * @param lowerBoundIndex
     * @param upperBoundIndex
     * @return
     */
    private Chromosome selectRandomIndividual(int lowerBoundIndex, int upperBoundIndex) {
        return population.get(
                RandomNumberGenerator.nextIntIn(lowerBoundIndex, upperBoundIndex)
        );
    }

    /**
     *
     */
    private void elitisticSave() {
        if (theBest == null) {
            theBest = population.get(0).deepCopy();
        } else {
            if (theBest.getFitness() > population.get(0).getFitness()) {
                theBest = population.get(0).deepCopy();
            }
        }
    }

    /**
     *
     */
    public void attachFitnessFunction(IFunction fitnessFunction) {
        this.fitnessFunction = Objects.requireNonNull(
                fitnessFunction, "Fitness function must not be null!"
        );
        for (Chromosome c : population) {
            c.setFitness(fitnessFunction.calcuateFitness(c.getBody()));
        }
        population.sort(Comparator.naturalOrder());
    }

}