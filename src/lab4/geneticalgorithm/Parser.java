package lab4.geneticalgorithm;

import lab4.crossover.Crossover;
import lab4.crossover.concrete.CombinedCrossover;
import lab4.crossover.concrete.InterpolationCrossover;
import lab4.crossover.concrete.StochasticInterpolationCrossover;
import lab4.mutation.Mutation;
import lab4.mutation.concrete.AdditionGaussian;
import lab4.mutation.concrete.CompositeMutationOperator;
import lab4.mutation.concrete.KGenesMutation;
import lab4.mutation.concrete.ReplacingGaussian;
import utils.various.Utils;

import java.util.*;

/**
 *
 * @author Danijel Pavlek
 */
public class Parser {

    /**
     *
     */
    private String content;

    /**
     * Required parameters:
     *      population_size
     *      lower_bounds
     *      upper_bounds
     *      mutation_rate
     *
     * @param content configuration
     */
    public Parser(String content) {
        this.content = Objects.requireNonNull(
                content, "Configuration must not be null!"
        );
    }

    /**
     *
     * @return
     */
    public GeneticAlgorithm parse() {
        String[] splittedByRows = content.split("\n");

        Map<String, String> mappedValues = new HashMap<>();
        for (String row : splittedByRows) {
            String[] brokenRow = row.replace(" ", "")
                    .replace("\t", "")
                    .split("=");
            if (brokenRow.length == 2 && !brokenRow[0].startsWith("#")) {
                mappedValues.put(brokenRow[0], brokenRow[1]);
            }
        }

        int populationSize = Integer.parseInt(mappedValues.get("population_size"));
        Crossover crossover = parseCrossover(mappedValues);
        Mutation mutation = parseMutation(mappedValues);
        boolean elitismEnabled = Boolean.parseBoolean(mappedValues.get("elitism_enabled"));
        parseVariableBounds(mappedValues);

        try {
            GeneticAlgorithm.survivorsRatio = Double.parseDouble(mappedValues.get("survivors_ratio"));
        } catch (Exception e) {

        }

        if (GeneticAlgorithm.survivorsRatio > 0.98 || GeneticAlgorithm.survivorsRatio < 0.1) {
            GeneticAlgorithm.survivorsRatio = 0.5;
        }

        return new GeneticAlgorithm(populationSize, elitismEnabled, crossover, mutation);
    }

    /**
     *
     * @param mappedValues
     */
    private void parseVariableBounds(Map<String, String> mappedValues) {
        parseBounds(mappedValues, "lower_bounds");
        parseBounds(mappedValues, "upper_bounds");
    }

    /**
     *
     * @param mappedValues
     * @param identifier
     */
    private void parseBounds(Map<String, String> mappedValues, String identifier) {
        double[] bounds;

        if (mappedValues.get(identifier).contains("*")) {
            String[] expression = mappedValues.get(identifier)
                    .replace(" ", "")
                    .split("\\*");

            int size = Integer.parseInt(expression[1]);

            double value = Double.parseDouble(
                    expression[0].replace("[", "")
                            .replace("]", "")
                            .replace(" ", "")
            );
            bounds = new double[size];
            for (int i = 0; i < size; ++i) {
                bounds[i] = value;
            }

        } else {
            bounds = Utils.parseArrayOfDoubles(mappedValues.get(identifier));
        }

        switch (identifier.toLowerCase()) {
            case "lower_bounds":
                Chromosome.lowerBounds = bounds;
                break;

            case "upper_bounds":
                Chromosome.upperBounds = bounds;
                break;

            default:
                throw new IllegalArgumentException(
                        "Unknown identifier: '" + identifier + "'. Allowed: 'lower_bounds', 'upper_bounds'."
                );
        }

    }

    private Crossover parseCrossover(Map<String, String> mappedValues) {
        String crossoverOperatorName = mappedValues.get("crossover_operator");
        if (crossoverOperatorName == null) {
            onInputMissing("crossover_operator");
        }

        double lambda = 0.5;
        if (mappedValues.containsKey("lambda")) {
            lambda = Double.parseDouble(mappedValues.get("lambda"));
        }

        int pointsToInterpolate = 1;
        if (mappedValues.containsKey("points_to_interpolate")) {
            lambda = Double.parseDouble(mappedValues.get("points_to_interpolate"));
        }

        switch (crossoverOperatorName) {
            case "interpolation":
                return new InterpolationCrossover(lambda);

            case "stochastic_interpolation":
                return new StochasticInterpolationCrossover(lambda);

            case "combined":
                return new CombinedCrossover(pointsToInterpolate);

            default:
                throw new IllegalArgumentException(
                        "Unknown crossover name: '" + crossoverOperatorName + "'."
                );
        }

    }

    private Mutation parseMutation(Map<String, String> mappedValues) {
        Mutation.mutationRate = Double.parseDouble(mappedValues.get("mutation_rate"));

        String mutationOperatorName = mappedValues.get("mutation_operator");
        if (mutationOperatorName == null) {
            onInputMissing("mutation_operator");

        } else if (mutationOperatorName.startsWith("[")) {
            String[] compositeOperators = mutationOperatorName
                    .substring(1, mutationOperatorName.length() - 1)
                    .split("\\),");
            List<Mutation> mutationOperators = new ArrayList<>(compositeOperators.length);

            double[] chances = new double[compositeOperators.length];

            for (int i = 0; i < compositeOperators.length; ++i) {
                String[] parameters = compositeOperators[i]
                        .replace(")", "")
                        .split("\\(")[1]
                        .split(",");

                switch (compositeOperators[i].toLowerCase().split("\\(")[0]) {

                    case "gaussianaddition":
                    case "gaussian_addition":
                        chances[i] = Double.parseDouble(parameters[0]);
                        mutationOperators.add(
                                parameters.length == 2 ?
                                        new AdditionGaussian(
                                                Double.parseDouble(parameters[1])
                                        ) :
                                        new AdditionGaussian(
                                                Double.parseDouble(parameters[1]),
                                                Double.parseDouble(parameters[2])
                                        )
                        );
                        break;

                    case "gaussianreplacing":
                    case "gaussian_replacing":
                        chances[i] = Double.parseDouble(parameters[0]);
                        mutationOperators.add(
                                parameters.length == 2 ?
                                        new ReplacingGaussian(
                                                Double.parseDouble(parameters[1])
                                        ) :
                                        new ReplacingGaussian(
                                                Double.parseDouble(parameters[1]),
                                                Double.parseDouble(parameters[2])
                                        )
                        );
                        break;
                }
            }

            Mutation[] parsedOperators = mutationOperators.toArray(new Mutation[2]);

            return new CompositeMutationOperator(chances, parsedOperators);

        }

        double intensity = mappedValues.containsKey("mutation_intensity") ?
                Double.parseDouble(mappedValues.get("mutation_intensity")) :
                1.0;

        int genesToMutate = mappedValues.containsKey("mutation_k") ?
                Integer.parseInt(mappedValues.get("mutation_k")) :
                1;

        switch (mutationOperatorName) {
            case "simple":
                return new AdditionGaussian(intensity);

            case "k_genes":
                return new KGenesMutation(genesToMutate, intensity);

            default:
                throw new IllegalArgumentException(
                        "Unknown mutation name: '" + mutationOperatorName + "'"
                );
        }

    }

    private void onInputMissing(String input) {
        throw new RuntimeException(
                "Input in configuration file missing: '" + input + "'"
        );
    }

}