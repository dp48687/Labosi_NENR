package lab4.mutation;

import lab4.geneticalgorithm.Chromosome;

/**
 *
 * @author Danijel Pavlek
 */
public abstract class Mutation {

    /**
     *
     */
    public static double mutationRate;

    /**
     *
     * @param c
     */
    public abstract void mutate(Chromosome c);

}