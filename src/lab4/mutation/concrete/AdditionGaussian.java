package lab4.mutation.concrete;

import lab4.geneticalgorithm.Chromosome;
import lab4.mutation.Mutation;
import utils.random.RandomNumberGenerator;

/**
 *
 * @author Danijel Pavlek
 */
public class AdditionGaussian extends Mutation {

    /**
     *
     */
    protected double standardDeviation;

    /**
     *
     */
    protected double mean;

    /**
     * Creates the default version of gaussian mutation, using standardDeviation 1.0
     */
    public AdditionGaussian() {
        this(1);
    }

    /**
     *
     * @param standardDeviation
     */
    public AdditionGaussian(double standardDeviation) {
        this(0, standardDeviation);
    }

    /**
     *
     * @param mean
     * @param standardDeviation
     */
    public AdditionGaussian(double mean, double standardDeviation) {
        this.standardDeviation = standardDeviation;
        this.mean = mean;
    }

    @Override
    public void mutate(Chromosome c) {
        for (int i = 0; i < Chromosome.upperBounds.length; ++i) {
            if (RandomNumberGenerator.nextDoubleIn(0, 1) < mutationRate) {
                mutateAt(c, i);
            }
        }
    }

    /**
     *
     * @param c
     * @param index
     */
    protected void mutateAt(Chromosome c, int index) {
        c.setGeneAt(index, c.getGeneAt(index) + RandomNumberGenerator.nextGaussian(mean, standardDeviation));
    }

}
