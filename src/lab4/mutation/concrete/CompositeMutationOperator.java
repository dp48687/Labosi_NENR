package lab4.mutation.concrete;

import lab4.geneticalgorithm.Chromosome;
import lab4.mutation.Mutation;
import utils.random.RandomNumberGenerator;

/**
 *
 * @author danci
 * @version 1.0
 */
public class CompositeMutationOperator extends Mutation {

    /**
     *
     */
    private Mutation[] mutationOperators;

    /**
     * Chances that specific operator will be selected.
     */
    private double[] chances;

    /**
     *
     * @param mutationOperators
     */
    public CompositeMutationOperator(double[] chances, Mutation... mutationOperators) {

        if (chances.length != mutationOperators.length) {
            throw new IllegalArgumentException(
                    "You provided wrong number of chances/operators: " +
                            mutationOperators.length + " operators and "
                            + chances.length + " chances."
            );
        }

        this.mutationOperators = mutationOperators;
        this.chances = chances;

    }

    @Override
    public void mutate(Chromosome c) {

        double determine = RandomNumberGenerator.nextDoubleIn(0, 1);
        double sum = 0;
        int index = 0;
        for (int i = 0; i < chances.length; ++i) {
            sum += chances[i];
            if (determine < sum) {
                index = i;
                break;
            }
        }

        Mutation selected = mutationOperators[index];
        selected.mutate(c);

    }


    /**
     * Maps values into interval [0, 1] so that they represent a valid discrete distribution.
     */
    private void normalizeSum() {
        double sum = 0;

        for (double chance : chances) {
            sum += chance;
        }

        if (Math.abs(sum - 1) > 1E-5) {
            for (int i = 0; i < chances.length; ++i) {
                chances[i] /= sum;
            }
        }
    }

}