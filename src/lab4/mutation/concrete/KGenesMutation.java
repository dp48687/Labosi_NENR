package lab4.mutation.concrete;

import lab4.geneticalgorithm.Chromosome;
import lab4.mutation.Mutation;
import utils.random.RandomNumberGenerator;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Danijel Pavlek
 */
public class KGenesMutation extends Mutation {

    /**
     * Number of genes to mutate.
     */
    private int k;

    /**
     *
     */
    private double intensity;

    /**
     *
     * @param k inside interval [1, number of variables]
     */
    public KGenesMutation(int k, double intensity) {
        this.k = k;
        this.intensity = intensity;
    }

    @Override
    public void mutate(Chromosome c) {
        Set<Integer> geneIndices = new HashSet<>();
        while (geneIndices.size() < k) {
            geneIndices.add(
                    RandomNumberGenerator.nextIntIn(
                            0, Chromosome.lowerBounds.length - 1
                    )
            );
        }

        geneIndices.forEach(geneIndex ->
                c.setGeneAt(
                        geneIndex,
                        c.getGeneAt(geneIndex) + RandomNumberGenerator.nextGaussian(0, intensity)
                )
        );

    }

}