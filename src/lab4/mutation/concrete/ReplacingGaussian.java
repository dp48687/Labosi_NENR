package lab4.mutation.concrete;

import lab4.geneticalgorithm.Chromosome;
import utils.random.RandomNumberGenerator;

/**
 *
 * @author danci
 * @version 1.0
 */
public class ReplacingGaussian extends AdditionGaussian {

    /**
     *
     */
    public ReplacingGaussian() {
        super(1);
    }

    /**
     *
     * @param standardDeviation
     */
    public ReplacingGaussian(double standardDeviation) {
        super(standardDeviation);
    }

    /**
     *
     * @param mean
     * @param standardDeviation
     */
    public ReplacingGaussian(double mean, double standardDeviation) {
        super(mean, standardDeviation);
    }

    @Override
    protected void mutateAt(Chromosome c, int index) {
        c.setGeneAt(index, RandomNumberGenerator.nextGaussian(mean, standardDeviation));
    }

}