package lab5.function;

import utils.various.Utils;

import java.util.Arrays;

public abstract class AbstractFunction {

    private double[] parameters;

    public AbstractFunction(double parameter) {
        parameters = new double[]{parameter};
    }

    public AbstractFunction(double... parameters) {
        this.parameters = Utils.copyArrayOfDoubles(parameters);
    }

    public abstract double calculate(double argument);

    public abstract double calculateDerivation(double argument);

    public abstract double calculateDerivationFromOutput(double argument);

    public double getParameter(int index) {
        return parameters[index];
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AbstractFunction that = (AbstractFunction) o;
        return Arrays.equals(parameters, that.parameters);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(parameters);
    }

}
