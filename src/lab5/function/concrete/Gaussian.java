package lab5.function.concrete;

import lab5.function.AbstractFunction;

public class Gaussian extends AbstractFunction {

    /**
     *
     * @param mu
     * @param sigmaSquared
     */
    public Gaussian(double mu, double sigmaSquared) {
        super(mu, sigmaSquared);
    }

    @Override
    public double calculate(double argument) {
        return Math.exp(
                -(argument - getParameter(0)) * (argument - getParameter(0)) / (2 * getParameter(1))
        );
    }

    @Override
    public double calculateDerivation(double argument) {
        throw new IllegalArgumentException("Not implemented yet");
    }

    @Override
    public double calculateDerivationFromOutput(double argument) {
        throw new IllegalArgumentException("Not implemented yet");
    }

}