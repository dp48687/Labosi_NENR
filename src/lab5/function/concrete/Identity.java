package lab5.function.concrete;

/**
 * This represents the identity function, f(x) = x
 */
public class Identity extends Linear {

    public Identity() {
        super(1.0, 0.0);
    }

    @Override
    public String toString() {
        return "identity";
    }

}
