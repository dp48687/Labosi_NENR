package lab5.function.concrete;

import lab5.function.AbstractFunction;

public class Linear extends AbstractFunction {

    /**
     * A linear function, f(x) = slope * x + shift | x in (-inf, inf)
     * @param slope steepness
     * @param shift shift
     */
    public Linear(double slope, double shift) {
        super(slope, shift, -Double.MAX_VALUE, Double.MAX_VALUE);
    }

    /**
     * A linear function, f(x) = slope * x + shift | x in (leftBound, rightBound), else 0
     * @param slope steepness
     * @param shift shift
     */
    public Linear(double slope, double shift, double leftBound, double rightBound) {
        super(slope, shift, leftBound, rightBound);
    }

    @Override
    public double calculate(double argument) {
        return argument > getParameter(2) && argument < getParameter(3) ?
                getParameter(0) * argument + getParameter(1) :
                0;
    }

    @Override
    public double calculateDerivation(double argument) {
        return argument > getParameter(2) && argument < getParameter(3) ?
                getParameter(0) :
                0;
    }

    @Override
    public double calculateDerivationFromOutput(double argument) {
        throw new UnsupportedOperationException(
                "This operation is not supported on this function."
        );
    }

    @Override
    public String toString() {
        return "linear";
    }

}
