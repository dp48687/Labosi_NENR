package lab5.function.concrete;

import lab5.function.AbstractFunction;

public class Sigmoid extends AbstractFunction {

    /**
     * Represents a function 1/(1+e^(-slope * x + shift))
     * @param slope
     * @param shift
     */
    public Sigmoid(double slope, double shift) {
        super(slope, shift);
    }

    @Override
    public double calculate(double argument) {
        return 1.0 / (1.0 + Math.exp(-argument * getParameter(0) + getParameter(1)));
    }

    @Override
    public double calculateDerivation(double argument) {
        throw new UnsupportedOperationException(
                "Use the method 'calculateDerivationFromOutput' instead!"
        );
    }

    @Override
    public double calculateDerivationFromOutput(double argument) {
        return getParameter(0) * argument * (1.0 - argument);
    }

    @Override
    public String toString() {
        return "sigmoid";
    }

}
