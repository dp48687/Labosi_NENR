package lab5.neuro.featurerecognition;

import lab5.neuro.trainingsamples.TrainingSample;

public class Gesture {

    private double[] inputs;

    private double[] outputs;

    private double[] xCoordinates;

    private double[] yCoordinates;


    public Gesture(TrainingSample ts) {
        this(new double[ts.getInputs().length / 2], new double[ts.getInputs().length / 2]);

        inputs = ts.getInputs();
        outputs = ts.getOutputs();

        for (int i = 0; i < xCoordinates.length; ++i) {
            xCoordinates[i] = ts.getInputs()[2 * i];
            yCoordinates[i] = ts.getInputs()[2 * i + 1];
        }

        normalize();
    }

    private Gesture(double[] xCoordinates, double[] yCoordinates) {
        this.xCoordinates = xCoordinates;
        this.yCoordinates = yCoordinates;
    }


    private double calculateTotalDistance() {
        double distance = 0;

        for (int i = 0; i < size() - 1; ++i) {
            distance += Math.sqrt(
                            (xCoordinates[i] - xCoordinates[i + 1]) * (xCoordinates[i] - xCoordinates[i + 1]) +
                            (yCoordinates[i] - yCoordinates[i + 1]) * (yCoordinates[i] - yCoordinates[i + 1])
            );
        }

        return distance;
    }

    private double[] calculatePartialDistances() {
        double[] distanceVector = new double[size() - 1];

        for (int i = 0; i < size() - 1; ++i) {
            double currentDistance = Math.sqrt(
                            (xCoordinates[i] - xCoordinates[i + 1]) * (xCoordinates[i] - xCoordinates[i + 1]) +
                            (yCoordinates[i] - yCoordinates[i + 1]) * (yCoordinates[i] - yCoordinates[i + 1])
            );
            distanceVector[i] = currentDistance;
        }

        return distanceVector;
    }

    private void normalize() {
        double[] averagePoint = calculateAveragePoint();
        translate(-averagePoint[0], -averagePoint[1]);
        scale(1.0 / getMaxCoordinate());
    }

    public void translate(double deltaX, double deltaY) {
        for (int i = 0; i < xCoordinates.length; ++i) {
            xCoordinates[i] += deltaX;
            yCoordinates[i] += deltaY;
        }
    }

    public void scale(double scaleFactor) {
        for (int i = 0; i < xCoordinates.length; ++i) {
            xCoordinates[i] *= scaleFactor;
            yCoordinates[i] *= scaleFactor;
        }
    }

    private double[] calculateAveragePoint() {
        double[] average = new double[]{0, 0};

        for (int i = 0; i < size(); ++i) {
            average[0] += xCoordinates[i];
            average[1] += yCoordinates[i];
        }

        average[0] /= size();
        average[1] /= size();

        return average;
    }

    private double getMaxCoordinate() {
        double max = 0;

        for (int i = 0; i < xCoordinates.length; ++i) {
            max = Math.max(max, Math.max(Math.abs(xCoordinates[i]), Math.abs(yCoordinates[i])));
        }

        return max;
    }

    public int size() {
        return xCoordinates.length;
    }

    public TrainingSample asTrainingSample(int M) {

        double[] inputs = new double[M * 2];
        double[] outputs = new double[5];

        System.arraycopy(this.outputs, 0, outputs, 0, outputs.length);

        double[] distanceVector = calculatePartialDistances();
        double distance = 0;
        for (int i = 0; i < distanceVector.length; ++i) {
            distance += distanceVector[i];
        }

        double scale = distance / (M - 1);
        for (int i = 0; i < M; ++i) {

            double distanceToNextPoint = scale * i;
            double currentDistance = 0;
            int currentPointIndex = -1;
            for (int j = 0; j < distanceVector.length; ++j) {
                if (Math.abs(distanceToNextPoint - currentDistance) <
                        Math.abs(distanceToNextPoint - (currentDistance + distanceVector[j]))) {
                    currentPointIndex = j;
                    break;
                } else {
                    currentDistance += distanceVector[j];
                }
            }

            if (currentPointIndex == -1) {
                currentPointIndex = distanceVector.length - 1;
            }

            inputs[2 * i] = xCoordinates[currentPointIndex];
            inputs[2 * i + 1] = yCoordinates[currentPointIndex];

        }

        return new TrainingSample(inputs, outputs);
    }

    @Override
    public String toString() {
        return asTrainingSample(10).toString();
    }

}