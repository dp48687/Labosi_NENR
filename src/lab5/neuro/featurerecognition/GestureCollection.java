package lab5.neuro.featurerecognition;

import lab5.neuro.trainingsamples.SampleParser;
import lab5.neuro.trainingsamples.TrainingSample;

import java.util.ArrayList;
import java.util.List;

public class GestureCollection {

    private List<Gesture> gestures;


    private GestureCollection(List<TrainingSample> samples) {
        gestures = new ArrayList<>(samples.size());
        samples.forEach(this::addSample);
    }


    public static GestureCollection createGestureCollection(String filePath) {
        return new GestureCollection(SampleParser.loadFromFile(filePath));
    }

    private void addSample(TrainingSample sample) {
        gestures.add(new Gesture(sample));
    }

    public List<TrainingSample> asTrainingSamples(int M) {
        List<TrainingSample> samples = new ArrayList<>(gestures.size());
        gestures.forEach(g -> samples.add(g.asTrainingSample(M)));
        return samples;
    }

}