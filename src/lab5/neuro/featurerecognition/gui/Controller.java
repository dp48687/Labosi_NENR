package lab5.neuro.featurerecognition.gui;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.stage.Stage;
import utils.various.FileIOUtils;
import utils.various.Utils;
import utils.various.WindowLauncherService;

import java.net.URL;
import java.text.DecimalFormat;
import java.util.ResourceBundle;

/**
 *
 * @author Danijel Pavlek
 * @version 1.0
 */
public class Controller implements Initializable {

    @FXML
    private GridPane root;

    @FXML
    private Canvas canvas;

    @FXML
    private Button predict, samplesPath;

    @FXML
    private RadioButton batch, minibatch;

    @FXML
    private TextField learningRate, groupSize, networkStructure, lambda;

    @FXML
    private Label prediction;

    private static final Color BACKGROUND_COLOR = Color.valueOf("#DBE8FF");
    private static final Color LINE_COLOR = Color.RED;
    private Model model = new Model(this);
    private GraphicsContext context;
    private boolean drawingEnabled;
    private DecimalFormat formatter = new DecimalFormat("#0.000000");
    private int iterationsProcessed = 0;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        context = canvas.getGraphicsContext2D();

        canvas.addEventHandler(MouseEvent.MOUSE_PRESSED, event -> {
            if (drawingEnabled) {
                if (model.gestureSize() > 0) {
                    model.reset();
                    clearBackground();
                }
                context.setStroke(Color.RED);
                context.beginPath();
                context.moveTo(event.getX(), event.getY());
                context.stroke();
                model.appendPoint(event.getX(), event.getY());
                drawConnectedPoints();
            }
        });

        canvas.addEventHandler(MouseEvent.MOUSE_DRAGGED, event -> {
            if (drawingEnabled) {
                context.setStroke(Color.RED);
                context.lineTo(event.getX(), event.getY());
                context.stroke();
                model.appendPoint(event.getX(), event.getY());
                drawConnectedPoints();
            }

        });

        canvas.addEventHandler(MouseEvent.MOUSE_RELEASED, event -> {
            drawingEnabled = false;
            model.buildGestureToPredict();
        });

        clearBackground();
    }

    private void clearBackground() {
        Paint fillBefore = context.getFill();

        context.setFill(BACKGROUND_COLOR);
        context.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());

        context.setFill(fillBefore);
        model.reset();

    }

    private void drawConnectedPoints() {
        Paint strokeBefore = context.getStroke();

        for (int i = 0; i < model.gestureSize() - 1; ++i) {
            context.setStroke(LINE_COLOR);
            context.strokeLine(
                    model.getXCoordinateAt(i), model.getYCoordinateAt(i),
                    model.getXCoordinateAt(i + 1), model.getYCoordinateAt(i + 1)
            );
        }

        context.setStroke(strokeBefore);
    }

    private String getSelectedText() {
        if (batch.isSelected()) {
            return "BATCH";
        } else if (minibatch.isSelected()) {
            return "MINIBATCH";
        } else {
            return "ONLINE";
        }
    }

    public void notifyAboutError() {
        if (iterationsProcessed++ % 100 == 0) {
            prediction.setText(
                            "ITERATION:                         " + (iterationsProcessed - 1) + ".\n" +
                            "TRAINING SET ERROR:        " + formatter.format(model.getLastTrainingError()) + '\n' +
                            "VALIDATION SET ERROR:    " + formatter.format(model.getLastTestError())
            );
        }
    }

    private void wrongValueMessage(String message, String provided) {
        WindowLauncherService.launchSimpleErrorDialog(
                "Wrong value!",
                message + " You provided: '" + provided + "'."
        );
    }

    @FXML
    public void onPathSelect() {
        samplesPath.setText(FileIOUtils.selectFilePath((Stage) (root.getScene().getWindow())));
    }

    @FXML
    public void onTrain() {
        boolean informationSetCorrectly = false;

        if (model.setNetworkStructure(networkStructure.getText())) {
            if (model.loadSamples(samplesPath.getText())) {

                double learningRate;
                try {
                    learningRate = Double.parseDouble(this.learningRate.getText());
                } catch (NumberFormatException notParsableNumber) {
                    wrongValueMessage(
                            "Learning rate must be a number.",
                            this.learningRate.getText()
                    );
                    return;
                }

                double lambda;
                try {
                    lambda = Double.parseDouble(this.lambda.getText());
                } catch (NumberFormatException notANumber) {
                    wrongValueMessage(
                            "Regularization factor must be a real value!",
                            this.lambda.getText()
                    );
                    return;
                }

                model.setLearningRate(learningRate);
                try {
                    informationSetCorrectly = model.setInfo(
                            getSelectedText(),
                            Integer.parseInt(groupSize.getText()),
                            lambda
                    );
                } catch (NumberFormatException e) {
                    if (getSelectedText().equals("MINIBATCH")) {
                        wrongValueMessage(
                                "Group size should be an integer from interval [2, 40]!", groupSize.getText()
                        );
                    } else {
                        informationSetCorrectly = model.setInfo(getSelectedText(), 0,lambda);
                    }
                }
            }
        }

        if (informationSetCorrectly) {
            model.train();
            drawingEnabled = true;
            predict.setDisable(false);
        }

    }

    @FXML
    public void onPredict() {
        if (model.gestureSize() > 0) {

            double[] predicted = model.predict();
            double ratio = Utils.max(predicted);
            int result = Utils.argmax(predicted);

            String resultLetter = "ALPHA";
            switch (result) {
                case 1:
                    resultLetter = "BETA";
                    break;

                case 2:
                    resultLetter = "GAMMA";
                    break;

                case 3:
                    resultLetter = "DELTA";
                    break;

                case 4:
                    resultLetter = "EPSILON";
            }

            prediction.setText(
                            "Predicted: " + resultLetter +
                            "\nPercentage: " +
                            formatter.format(100 * ratio) +
                            " %"
            );

            drawingEnabled = true;
        }
    }

    @FXML
    public void onClearBackground() {
        clearBackground();
    }

    @FXML
    public void onExit() {
        Platform.exit();
    }

}