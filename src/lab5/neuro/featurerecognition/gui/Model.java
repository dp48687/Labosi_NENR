package lab5.neuro.featurerecognition.gui;

import javafx.application.Platform;
import lab5.neuro.featurerecognition.Gesture;
import lab5.neuro.featurerecognition.GestureCollection;
import lab5.neuro.learningalgorithms.AbstractNNLearningAlgorithm;
import lab5.neuro.learningalgorithms.backpropagation.BatchBackpropagation;
import lab5.neuro.learningalgorithms.backpropagation.MinibatchBackpropagation;
import lab5.neuro.learningalgorithms.backpropagation.OnlineBackpropagation;
import lab5.neuro.neuroskeleton.NeuralNetwork;
import lab5.neuro.trainingsamples.TrainingSample;
import utils.various.WindowLauncherService;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Danijel Pavlek
 * @version 1.0
 */
public class Model {

    private GestureCollection gestures;

    private AbstractNNLearningAlgorithm algorithm;
    private int groupSize;
    private double learningRate;
    private List<TrainingSample> samples;
    private final static int ITERATIONS = 20_000;

    private NeuralNetwork neuralNetwork;
    private int inputNeurons;
    private int[] hiddenNeuronsPerLayer = new int[]{15};

    private TrainingSample toPredict;
    private List<Double> xCoordinates = new ArrayList<>(500);
    private List<Double> yCoordinates = new ArrayList<>(500);

    private Controller controller;


    public Model(Controller controller) {
        this.controller = controller;
    }


    public double getXCoordinateAt(int index) {
        return xCoordinates.get(index);
    }

    public double getYCoordinateAt(int index) {
        return yCoordinates.get(index);
    }

    public void appendPoint(double xCoordinate, double yCoordinate) {
        xCoordinates.add(xCoordinate);
        yCoordinates.add(yCoordinate);
    }

    public int gestureSize() {
        return xCoordinates.size();
    }

    public void reset() {
        xCoordinates.clear();
        yCoordinates.clear();
    }

    public boolean setInfo(String buttonText,
                           int groupSize,
                           double regularizationFactor) {

        samples = gestures.asTrainingSamples(inputNeurons / 2);

        StringBuilder networkStructureBuilder = new StringBuilder(7);
        networkStructureBuilder.append(inputNeurons);
        networkStructureBuilder.append('x');
        for (int i = 0; i < hiddenNeuronsPerLayer.length; ++i) {
            networkStructureBuilder.append(hiddenNeuronsPerLayer[i]);
            networkStructureBuilder.append('x');
        }
        networkStructureBuilder.append('5');

        neuralNetwork = new NeuralNetwork(networkStructureBuilder.toString());

        switch (buttonText.toUpperCase()) {
            case "ONLINE":
                algorithm = new OnlineBackpropagation(
                        neuralNetwork, samples, learningRate, regularizationFactor
                );
                return true;

            case "BATCH":
                algorithm = new BatchBackpropagation(
                        neuralNetwork, samples, learningRate, regularizationFactor
                );
                return true;

            case "MINIBATCH":
                if (groupSize < 2 || groupSize > 40) {
                    WindowLauncherService.launchSimpleErrorDialog(
                            "Wrong value of M!",
                            "You provided wrong value for group size! Allowed: [2, 40]."
                    );
                    return false;

                } else {
                    this.groupSize = groupSize;
                    algorithm = new MinibatchBackpropagation(
                            neuralNetwork, samples, learningRate, groupSize, regularizationFactor
                    );
                    return true;
                }

            default:
                throw new IllegalArgumentException(
                        "Wrong button text: " + buttonText
                );
        }

    }

    public boolean setNetworkStructure(String networkStructure) {
        boolean correctDefinition = true;

        String[] splitted = networkStructure.split("x");
        int[] structure = new int[splitted.length];
        for (int i = 0; i < splitted.length; ++i) {
            try {
                structure[i] = Integer.parseInt(splitted[i]);
            } catch (NumberFormatException notAnInteger) {
                correctDefinition = false;
                break;
            }
        }

        correctDefinition = correctDefinition && structure.length > 2;
        if (!correctDefinition) {
            WindowLauncherService.launchSimpleErrorDialog(
                    "Wrong input data!",
                    "Network structure should be defined as number of neurons in each layer separated by x." +
                            " Also, it must have at least one hidden layer."
            );
            return false;
        }

        if (structure[0] < 10 || structure[0] > 60 || structure[0] % 2 == 1) {
            WindowLauncherService.launchSimpleErrorDialog(
                    "Wrong input data!",
                    "Number of input neurons should be an even integer from interval [10, 60]."
            );
            return false;
        } else {
            this.inputNeurons = structure[0];
        }

        return true;
    }

    public boolean loadSamples(String path) {
        try {
            gestures = GestureCollection.createGestureCollection(path);
            return true;

        } catch (Exception e) {
            return false;
        }
    }

    public void setLearningRate(double learningRate) {
        this.learningRate = learningRate;
    }

    public double getLastTrainingError() {
        return algorithm.getLastTrainingError();
    }

    public double getLastTestError() {
        return algorithm.getLastTestError();
    }

    public void train() {
        Thread t = new Thread(() -> {
            for (int i = 0; i < ITERATIONS; ++i) {
                algorithm.processOneEpoch();
                Platform.runLater(controller::notifyAboutError);
            }
        });
        t.setDaemon(true);
        t.start();
    }

    public double[] predict() {
        return neuralNetwork.calculateOutputs(toPredict.getInputs());
    }

    public void buildGestureToPredict() {
        if (xCoordinates.size() >= 5) {
            double[] inputs = new double[xCoordinates.size() * 2];

            for (int i = 0; i < xCoordinates.size(); ++i) {
                inputs[2 * i] = xCoordinates.get(i);
                inputs[2 * i + 1] = yCoordinates.get(i);
            }

            toPredict = new Gesture(
                    new TrainingSample(inputs, new double[]{0, 0, 0, 0, 0})
            ).asTrainingSample(inputNeurons / 2);
        }

    }

}