package lab5.neuro.learningalgorithms;

import lab5.neuro.neuroskeleton.NeuralNetwork;
import lab5.neuro.trainingsamples.SampleParser;
import lab5.neuro.trainingsamples.TrainingSample;
import utils.various.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public abstract class AbstractNNLearningAlgorithm {
    public static final float TRAINING_SAMPLE_SIZE_PERCENTAGE = 0.9f;

    protected NeuralNetwork willBeTrained;
    protected List<TrainingSample> trainingSamples;
    protected List<TrainingSample> validationSamples;
    protected List<Double> mseErrorTrainingSet = new ArrayList<>(1000);
    protected List<Double> mseErrorValidationSet = new ArrayList<>(1000);
    protected double learningRate;

    public AbstractNNLearningAlgorithm(NeuralNetwork willBeTrained,
                                       List<TrainingSample> samples,
                                       double learningRate) {
        Objects.requireNonNull(samples);

        List<TrainingSample>[] splitted = SampleParser.split(
                samples, (int) ((double) samples.size() * TRAINING_SAMPLE_SIZE_PERCENTAGE), false
        );

        this.trainingSamples = splitted[0];
        this.validationSamples = splitted[1];
        this.willBeTrained = willBeTrained;
        this.learningRate = learningRate;
    }

    public abstract void processOneEpoch();

    private void processNEpochs(int n) {
        onTrainingStarted();
        for (int i = 0; i < n; ++i) {
            onEpochStarted(i);
            processOneEpoch();
            onEpochFinished(i);
        }
        onTrainingFinished();
    }

    public final void runTraining(int epochs) {
        processNEpochs(epochs);
    }

    protected void onTrainingStarted() {

    }

    protected void onTrainingFinished() {

    }

    protected void onEpochStarted(int epochIndex) {

    }

    protected void onEpochFinished(int epochIndex) {

    }

    protected void registerMSETrainingError(double error) {
        mseErrorTrainingSet.add(error);
    }

    protected void registerMSEValidationError(double error) {
        mseErrorValidationSet.add(error);
    }

    /**
     * Calculates MSE on validation set.
     * @return 1.0/(2 * len(validationSet) * )
     */
    protected double getValidationMSE() {
        double accumulated = 0.0;

        for (TrainingSample validationSample : validationSamples) {
            double[] calculated = willBeTrained.calculateOutputs(validationSample.getInputs());
            accumulated += Utils.calculateSquaredDifference(calculated, validationSample.getOutputs());
        }

        return accumulated / (2.0 * validationSamples.size());
    }

    public void printCurrentErrors() {
        if (!mseErrorValidationSet.isEmpty() && !mseErrorTrainingSet.isEmpty()) {
            System.out.println(
                            "TRAINING ERROR: " +
                            mseErrorTrainingSet.get(mseErrorTrainingSet.size() - 1) +
                            "  VALIDATION ERROR: " +
                            mseErrorValidationSet.get(mseErrorValidationSet.size() - 1)

            );
        }
    }

    public List<Double> getMseErrorTrainingSet() {
        return mseErrorTrainingSet;
    }

    public List<Double> getMseErrorValidationSet() {
        return mseErrorValidationSet;
    }

    public double getLastTrainingError() {
        return mseErrorTrainingSet.get(mseErrorTrainingSet.size() - 1);
    }

    public double getLastTestError() {
        return mseErrorValidationSet.get(mseErrorValidationSet.size() - 1);
    }

}
