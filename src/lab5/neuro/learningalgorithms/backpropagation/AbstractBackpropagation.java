package lab5.neuro.learningalgorithms.backpropagation;

import lab5.function.AbstractFunction;
import lab5.function.concrete.Sigmoid;
import lab5.neuro.learningalgorithms.AbstractNNLearningAlgorithm;
import lab5.neuro.neuroskeleton.NeuralNetwork;
import lab5.neuro.neuroskeleton.layer.Layer;
import lab5.neuro.trainingsamples.TrainingSample;
import utils.various.Utils;

import java.util.List;

/**
 *
 * @author Danijel Pavlek
 * @version 1.0
 */
public abstract class AbstractBackpropagation extends AbstractNNLearningAlgorithm {

    protected double lambda;

    public AbstractBackpropagation(NeuralNetwork willBeTrained,
                                   List<TrainingSample> samples,
                                   double learningRate) {
        super(willBeTrained, samples, learningRate);
    }

    public AbstractBackpropagation(NeuralNetwork willBeTrained,
                                   List<TrainingSample> samples,
                                   double learningRate,
                                   double lambda) {
        this(willBeTrained, samples, learningRate);
        this.lambda = lambda;
    }

    protected double processOneSample(TrainingSample ts, double[] accumulatedCorrection) {
        double trainingError = 0.0;

        double[] results = willBeTrained.calculateOutputs(ts.getInputs());
        trainingError += Utils.calculateSquaredDifference(results, ts.getOutputs());

        calculateCorrectionInLastLayer(willBeTrained.getLayers(), ts, accumulatedCorrection);
        Layer[] layers = willBeTrained.getLayers();
        int numberOfLayers = willBeTrained.getLayers().length;
        int correctionsCalculated = (layers[numberOfLayers - 1].getInputs().length + 1) *
                layers[numberOfLayers - 1].getOutputs().length;
        for (int i = willBeTrained.getLayers().length - 2; i > 0; --i) {
            calculateCorrectionInMiddleLayer(willBeTrained.getLayers(), i, accumulatedCorrection, correctionsCalculated);
            correctionsCalculated += (layers[i].getInputs().length + 1) * layers[i].getOutputs().length;
        }

        return trainingError;
    }

    protected void calculateCorrectionInLastLayer(Layer[] layers,
                                                  TrainingSample ts,
                                                  double[] accumulatedCorrection) {

        Layer current = layers[layers.length - 1];

        calculateDeltasInLastLayer(ts, current);
        int totalWeights = accumulatedCorrection.length;
        int weightsToProcess = (current.getInputs().length + 1) * current.getOutputs().length;
        for (int x = 0; x < current.getInputs().length + 1; ++x) {
            double input = x == current.getInputs().length ? -1.0 : current.getInputs()[x];

            for (int y = 0; y < current.getOutputs().length; ++y) {
                accumulatedCorrection[totalWeights - weightsToProcess--] = -current.getDeltas()[y] * input * learningRate;
            }
        }

    }

    protected void calculateCorrectionInMiddleLayer(Layer[] layers,
                                                    int currentLayerIndex,
                                                    double[] accumulatedCorrection,
                                                    int correctionsAlreadyCalculated) {

        Layer current = layers[currentLayerIndex];
        double[] deltas = current.getDeltas();

        calculateDeltasInMiddleLayer(current, layers[currentLayerIndex + 1]);

        double[] inputs = current.getInputs();
        int totalWeights = accumulatedCorrection.length;
        int weightsToProcess = (current.getInputs().length + 1) * current.getOutputs().length;
        for (int x = 0; x < current.getInputs().length + 1; ++x) {
            for (int y = 0; y < current.getOutputs().length; ++y) {
                double input = x == current.getInputs().length ? -1.0 : inputs[x];
                accumulatedCorrection[totalWeights - correctionsAlreadyCalculated - weightsToProcess--] = -deltas[y] * input * learningRate;
            }
        }

    }

    protected void calculateDeltasInLastLayer(TrainingSample ts, Layer current) {

        double[] outputs = current.getOutputs();
        double[] deltas = current.getDeltas();
        AbstractFunction[] activationFunctions = current.getActivationFunctions();

        for (int j = 0; j < outputs.length; ++j) {
            double relativeDiff = (outputs[j] - ts.outputAt(j));
            if (activationFunctions[j] instanceof Sigmoid) {
                deltas[j] = activationFunctions[j].calculateDerivationFromOutput(outputs[j]) * relativeDiff;
            } else {
                deltas[j] = activationFunctions[j].calculateDerivation(current.getNetSums()[j]) * relativeDiff;
            }
        }
    }

    protected void calculateDeltasInMiddleLayer(Layer current, Layer next) {

        double[] outputs = current.getOutputs();
        double[] deltas = current.getDeltas();
        AbstractFunction[] activationFunctions = current.getActivationFunctions();

        for (int j = 0; j < outputs.length; ++j) {
            if (activationFunctions[j] instanceof Sigmoid) {
                deltas[j] = activationFunctions[j].calculateDerivationFromOutput(outputs[j]);
            } else {
                deltas[j] = activationFunctions[j].calculateDerivation(current.getNetSums()[j]);
            }
        }

        for (int y = 0; y < current.getOutputs().length; ++y) {

            double[] nextOutputs = next.getOutputs();
            double[] nextDeltas = next.getDeltas();
            double[][] nextWeights = next.getWeights();
            double accumulated = 0.0;
            for (int yNext = 0; yNext < nextOutputs.length; ++yNext) {
                accumulated += nextDeltas[yNext] * nextWeights[y][yNext];
            }
            deltas[y] *= accumulated;

        }

    }

    /**
     *
     * @param accumulatedCorrection
     * @param lambda regularization factor
     */
    protected void correctWeights(double[] accumulatedCorrection, double lambda) {
        for (int i = 0; i < willBeTrained.getWeights().length; ++i) {
            willBeTrained.getWeights()[i] = willBeTrained.getWeights()[i] +
                                            accumulatedCorrection[i] -
                                            willBeTrained.getWeights()[i] * lambda;
        }

        willBeTrained.assignWeights(willBeTrained.getWeights());
    }

}
