package lab5.neuro.learningalgorithms.backpropagation;

import lab5.neuro.neuroskeleton.NeuralNetwork;
import lab5.neuro.trainingsamples.TrainingSample;

import java.util.Collections;
import java.util.List;

/**
 *
 * @author Danijel Pavlek
 * @version 1.0
 */
public class BatchBackpropagation extends AbstractBackpropagation {

    public BatchBackpropagation(NeuralNetwork willBeTrained,
                                List<TrainingSample> samples,
                                double learningRate) {
        super(willBeTrained, samples, learningRate);
    }

    public BatchBackpropagation(NeuralNetwork willBeTrained,
                                List<TrainingSample> samples,
                                double learningRate,
                                double lambda) {
        super(willBeTrained, samples, learningRate, lambda);
    }

    @Override
    public void processOneEpoch() {
        double trainingError = 0.0;

        double[] accumulatedCorrection = new double[willBeTrained.getWeights().length];

        willBeTrained.updateWeights();
        for (TrainingSample ts: trainingSamples) {
            trainingError += processOneSample(ts, accumulatedCorrection);
        }

        correctWeights(accumulatedCorrection, lambda);

        Collections.shuffle(trainingSamples);

        registerMSETrainingError(trainingError / trainingSamples.size());
        registerMSEValidationError(getValidationMSE());

    }

}
