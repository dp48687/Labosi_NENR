package lab5.neuro.learningalgorithms.backpropagation;

import lab5.neuro.neuroskeleton.NeuralNetwork;
import lab5.neuro.trainingsamples.TrainingSample;

import java.util.Collections;
import java.util.List;

public class MinibatchBackpropagation extends AbstractBackpropagation {

    private int batchSize;

    public MinibatchBackpropagation(NeuralNetwork willBeTrained,
                                    List<TrainingSample> samples,
                                    double learningRate,
                                    int batchSize) {
        super(willBeTrained, samples, learningRate);
        this.batchSize = batchSize;
    }

    public MinibatchBackpropagation(NeuralNetwork willBeTrained,
                                    List<TrainingSample> samples,
                                    double learningRate,
                                    int batchSize,
                                    double lambda) {
        super(willBeTrained, samples, learningRate, lambda);
        this.batchSize = batchSize;
    }

    @Override
    public void processOneEpoch() {
        double trainingError = 0.0;
        double[] accumulatedCorrection = new double[willBeTrained.getWeights().length];

        for (int i = 0; i < batchSize; ++i) {

            willBeTrained.updateWeights();

            for (int j = i * batchSize; j < Math.min((i + 1) * batchSize, trainingSamples.size()); ++j) {
                trainingError += processOneSample(trainingSamples.get(j), accumulatedCorrection);
            }

            correctWeights(accumulatedCorrection, lambda);

        }

        Collections.shuffle(trainingSamples);

        registerMSETrainingError(trainingError / trainingSamples.size());
        registerMSEValidationError(getValidationMSE());

    }

}
