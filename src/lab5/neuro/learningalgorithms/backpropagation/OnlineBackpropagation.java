package lab5.neuro.learningalgorithms.backpropagation;

import lab5.neuro.neuroskeleton.NeuralNetwork;
import lab5.neuro.trainingsamples.TrainingSample;

import java.util.List;

public class OnlineBackpropagation extends AbstractBackpropagation {

    public OnlineBackpropagation(NeuralNetwork willBeTrained,
                                 List<TrainingSample> samples,
                                 double learningRate) {
        super(willBeTrained,
                samples,
                learningRate
        );
    }

    public OnlineBackpropagation(NeuralNetwork willBeTrained,
                                 List<TrainingSample> samples,
                                 double learningRate,
                                 double lambda) {
        super(willBeTrained, samples, learningRate, lambda);

    }

    @Override
    public void processOneEpoch() {

        double trainingError = 0.0;
        double[] accumulatedCorrection = new double[willBeTrained.getWeights().length];

        for (TrainingSample ts: trainingSamples) {
            willBeTrained.updateWeights();

            trainingError += processOneSample(ts, accumulatedCorrection);

            correctWeights(accumulatedCorrection, lambda);

        }

        registerMSETrainingError(trainingError / (trainingSamples.size()));
        registerMSEValidationError(getValidationMSE());

    }

}
