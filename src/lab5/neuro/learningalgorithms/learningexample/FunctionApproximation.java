package lab5.neuro.learningalgorithms.learningexample;

import lab5.neuro.learningalgorithms.AbstractNNLearningAlgorithm;
import lab5.neuro.learningalgorithms.backpropagation.OnlineBackpropagation;
import lab5.neuro.neuroskeleton.NeuralNetwork;
import lab5.neuro.trainingsamples.TrainingSample;
import utils.random.RandomNumberGenerator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FunctionApproximation {

    private static float[] lowerBoundsInput = new float[]{-1, -1};
    private static float[] upperBoundsInput = new float[]{1, 1};

    private static double functionToApproximate(double[] argument) {
        double result = 0.0;
        for (int i = 0; i < argument.length; ++i) {
            result += (argument[i] - 0.01 * i) * i;
        }
        return result;
    }

    private static TrainingSample generateRandomSample() {
        double[] inputs = new double[lowerBoundsInput.length];
        for (int i = 0; i < inputs.length; ++i) {
            inputs[i] = RandomNumberGenerator.nextDoubleIn(lowerBoundsInput[i], upperBoundsInput[i]);
        }
        return new TrainingSample(inputs, new double[]{functionToApproximate(inputs)});
    }

    public static void main(String[] args) {
        int samplesToGenerate = 1000;
        List<TrainingSample> samples = new ArrayList<>(samplesToGenerate);
        for (int i = 0; i < samplesToGenerate; ++i) {
            samples.add(generateRandomSample());
        }

        NeuralNetwork neuralNetwork = new NeuralNetwork("2x3x1");
        AbstractNNLearningAlgorithm algorithm = new OnlineBackpropagation(neuralNetwork, samples, 1.0 / (2.0 * samplesToGenerate));

        for (int i = 0; i < 100000; ++i) {
            algorithm.runTraining(10);
            if (i % 1000 == 0) {
                System.out.println(algorithm.getLastTrainingError());
            }
        }

        System.out.println(Arrays.toString(neuralNetwork.calculateOutputs(samples.get(0).getInputs())));
        System.out.println(Arrays.toString(neuralNetwork.calculateOutputs(samples.get(1).getInputs())));
        System.out.println(Arrays.toString(neuralNetwork.calculateOutputs(samples.get(2).getInputs())));
        System.out.println(Arrays.toString(neuralNetwork.calculateOutputs(samples.get(3).getInputs())));
    }

}
