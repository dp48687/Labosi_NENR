package lab5.neuro.learningalgorithms.learningexample;

import lab5.neuro.learningalgorithms.AbstractNNLearningAlgorithm;
import lab5.neuro.learningalgorithms.backpropagation.OnlineBackpropagation;
import lab5.neuro.neuroskeleton.NeuralNetwork;
import lab5.neuro.trainingsamples.TrainingSample;

import java.util.Arrays;
import java.util.List;

public class XorProblem {

    private static NeuralNetwork neuralNetwork = new NeuralNetwork("2x3x1");
    private static List<TrainingSample> samples = Arrays.asList(
            new TrainingSample(new double[]{0, 0}, new double[]{0}),
            new TrainingSample(new double[]{0, 1}, new double[]{1}),
            new TrainingSample(new double[]{1, 0}, new double[]{1}),
            new TrainingSample(new double[]{1, 1}, new double[]{0})
    );
    private static AbstractNNLearningAlgorithm algorithm = new OnlineBackpropagation(neuralNetwork, samples, 0.125);

    public static void main(String[] args) {
        for (int i = 0; i < 100000; ++i) {
            algorithm.runTraining(10);
            if (i % 1000 == 0) {
                System.out.println(algorithm.getLastTrainingError());
            }
        }
        System.out.println(Arrays.toString(neuralNetwork.calculateOutputs(samples.get(0).getInputs())));
        System.out.println(Arrays.toString(neuralNetwork.calculateOutputs(samples.get(1).getInputs())));
        System.out.println(Arrays.toString(neuralNetwork.calculateOutputs(samples.get(2).getInputs())));
        System.out.println(Arrays.toString(neuralNetwork.calculateOutputs(samples.get(3).getInputs())));
    }

}
