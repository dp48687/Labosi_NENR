package lab5.neuro.neuroskeleton;

import lab5.neuro.neuroskeleton.layer.InputLayer;
import lab5.neuro.neuroskeleton.layer.Layer;
import lab5.neuro.neuroskeleton.layer.SigmoidLayer;
import lab5.neuro.neuroskeleton.layer.Type1Layer;
import utils.various.Utils;

import java.util.Arrays;

public class NeuralNetwork {

    /**
     * Lazy initialized set of weights.
     */
    private double[] weights;

    /**
     * Layers of this neural network.
     * 0-th layer - input layer - contains no weights, uses identity as an
     *              activation function
     *
     */
    private Layer[] layers;

    private NeuralNetwork() {

    }

    public NeuralNetwork(String structure) {
        int[] layerStructure = parseLayerStructure(structure);
        buildLayers(layerStructure);
    }

    private int[] parseLayerStructure(String structure) {
        String[] splitted = structure
                .replace(" ", "")
                .trim()
                .toLowerCase()
                .split("x");

        int[] layerStructure = new int[splitted.length];
        for (int index = 0; index < layerStructure.length; ++index) {
            layerStructure[index] = Integer.parseInt(splitted[index]);
        }

        return layerStructure;
    }

    private void buildLayers(int[] layerStructure) {
        layers = new Layer[layerStructure.length];

        layers[0] = new InputLayer(layerStructure[0]);
        for (int i = 0; i < layerStructure.length - 1; ++i) {
            layers[i + 1] = layerStructure[i + 1] > 0 ?
                    new SigmoidLayer(Math.abs(layerStructure[i]), Math.abs(layerStructure[i + 1])) :
                    new Type1Layer(Math.abs(layerStructure[i]), Math.abs(layerStructure[i + 1]));
        }

    }

    public Layer[] getLayers() {
        return layers;
    }

    public double[] calculateOutputs(double[] inputs) {
        double[] result = new double[layers[layers.length - 1].getOutputs().length];
        calculateIntoField(inputs, result);
        return result;
    }

    public void calculateIntoField(double[] inputs, double[] storeInto) {
        layers[0].setInputs(inputs);
        for (int i = 0; i < layers.length - 1; ++i) {
            layers[i].calculateOutputs();
            layers[i + 1].setInputs(layers[i].getOutputs());
        }
        layers[layers.length - 1].setInputs(layers[layers.length - 2].getOutputs());
        layers[layers.length - 1].calculateOutputs();

        System.arraycopy(layers[layers.length - 1].getOutputs(), 0, storeInto, 0, storeInto.length);
    }

    public NeuralNetwork deepCopy() {
        NeuralNetwork clone = new NeuralNetwork();

        Layer[] layers = new Layer[this.layers.length];
        for (int i = 0; i < this.layers.length; ++i) {
            layers[i] = this.layers[i].deepCopy();
        }
        clone.layers = layers;

        return clone;
    }

    public void assignWeights(double[] weights) {
        int weightCounter = 0;
        for (int i = 1; i < layers.length; ++i) {
            int inputs = layers[i].getInputs().length + 1;
            if (layers[i] instanceof Type1Layer) {
                --inputs;
            }
            int outputs = layers[i].getOutputs().length;
            layers[i].setWeights(weights, weightCounter);
            weightCounter += inputs * outputs;
        }

        for (Layer l : layers) {
            if (l instanceof Type1Layer) {
                Type1Layer t1 = (Type1Layer) l;
                t1.setNormalizationFactors(weights, weightCounter);
                weightCounter += t1.getNormalizationFactors().length;
            }
        }

    }

    /**
     * Copies the weights from layers into 1D array.
     */
    public void updateWeights() {

        if (weights == null) {
            int weightCounter = 0;
            for (int i = 1; i < layers.length; ++i) {
                weightCounter += ((layers[i].getWeights().length) * layers[i].getWeights()[0].length);
                if (layers[i] instanceof Type1Layer) {
                    weightCounter += ((Type1Layer) layers[i]).getNormalizationFactors().length;
                }
            }
            weights = new double[weightCounter];
        }

        int weightCounter = 0;
        for (int i = 1; i < layers.length; ++i) {
            int inputs = layers[i] instanceof Type1Layer ?
                    layers[i].getInputs().length :
                    layers[i].getInputs().length + 1;
            int outputs = layers[i].getOutputs().length;
            for (int x = 0; x < inputs; ++x) {
                for (int y = 0; y < outputs; ++y) {
                    weights[weightCounter++] = layers[i].getWeights()[x][y];
                }
            }
        }

        for (Layer l : layers) {
            if (l instanceof Type1Layer) {
                Type1Layer t1 = (Type1Layer) l;
                for (int i = 0; i < t1.getNormalizationFactors().length; ++i) {
                    weights[weightCounter++] = t1.getNormalizationFactors()[i];
                }
            }
        }

    }

    public double[] getWeights() {
        if (weights == null) {
            updateWeights();
        }
        return weights;
    }

    public static NeuralNetwork buildFromArray(double[] weights, String structure) {
        NeuralNetwork created = new NeuralNetwork(structure);
        created.assignWeights(weights);

        return created;
    }

    /**
     *
     * @param strRepresentation
     * @return
     */
    public static NeuralNetwork fromString(String strRepresentation) {
        String[] broken = strRepresentation.split("\n");
        return buildFromArray(Utils.parseArrayOfDoubles(broken[1]), broken[0]);
    }

    @Override
    public String toString() {
        StringBuilder netStringBuilder = new StringBuilder();

        netStringBuilder.append(layers[0].getOutputs().length);
        netStringBuilder.append("x");
        for (int i = 1; i < layers.length; ++i) {
            int outputs = layers[i].getOutputs().length;
            if (layers[i] instanceof Type1Layer) {
                outputs *= -1;
            }
            netStringBuilder.append(outputs);

            netStringBuilder.append(i < layers.length - 1 ? 'x' : '\n');
        }

        updateWeights();
        netStringBuilder.append(Arrays.toString(getWeights()));

        return netStringBuilder.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NeuralNetwork that = (NeuralNetwork) o;
        return Arrays.equals(layers, that.layers);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(layers);
    }

    public static void main(String[] args) {
        NeuralNetwork nn = new NeuralNetwork("2x3x1");

        System.out.println(
                Arrays.toString(
                        nn.calculateOutputs(new double[]{0, 1})
                )
        );

    }

}
