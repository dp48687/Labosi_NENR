package lab5.neuro.neuroskeleton.layer;

import lab5.function.AbstractFunction;
import utils.various.Utils;

public class InputLayer extends Layer {

    public InputLayer(int neurons) {
        super();
        this.inputs = new double[neurons];
        this.outputs = new double[neurons];
    }

    @Override
    public void calculateOutputs() {
        System.arraycopy(inputs, 0, outputs, 0, inputs.length);
    }

    @Override
    protected void init(int neurons, int nextLayerNeurons, AbstractFunction[] activationFunctions) {

    }

    @Override
    public Layer deepCopy() {
        Layer clone = new InputLayer(inputs.length);

        clone.inputs = Utils.copyArrayOfDoubles(inputs);
        clone.outputs = Utils.copyArrayOfDoubles(outputs);


        return clone;
    }

}
