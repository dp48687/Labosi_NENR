package lab5.neuro.neuroskeleton.layer;

import lab5.function.AbstractFunction;
import utils.random.RandomNumberGenerator;
import utils.various.Utils;

import java.util.Arrays;

public class Layer {
    protected static final float WEIGHTS_LOWER_BOUND = -1f;
    protected static final float WEIGHTS_UPPER_BOUND = 1f;

    protected double[] inputs;
    protected double[] netSums;
    protected double[] outputs;
    protected double[] deltas;
    protected double[][] weights;
    protected AbstractFunction[] activationFunctions;

    protected Layer() {

    }

    public Layer(int neurons, int nextLayerNeurons, AbstractFunction[] activationFunctions) {
        init(neurons, nextLayerNeurons, activationFunctions);
    }

    protected void init(int neurons, int nextLayerNeurons, AbstractFunction[] activationFunctions) {
        inputs = new double[neurons];
        outputs = new double[nextLayerNeurons];
        netSums = new double[nextLayerNeurons];
        deltas = new double[outputs.length];
        if (nextLayerNeurons > 0) {
            weights = new double[neurons + 1][nextLayerNeurons];
        }
        this.activationFunctions = activationFunctions;
        assignRandomWeights(WEIGHTS_LOWER_BOUND, WEIGHTS_UPPER_BOUND);
    }

    public void assignWeightsToOne() {
        System.err.println("Assigned weights to 1.0");
        for (int i = 0; i < weights.length; ++i) {
            for (int j = 0; j < weights[i].length; ++j) {
                weights[i][j] = 1.0;
            }
        }
    }

    public void assignRandomWeights(float lowerBound, float upperBound) {
        for (int i = 0; i < weights.length; ++i) {
            weights[i] = RandomNumberGenerator.nextDoubleVector(
                    weights[i].length,
                    lowerBound,
                    upperBound
            );
        }
    }

    public void setInputs(double[] inputs) {
        System.arraycopy(inputs, 0, this.inputs, 0, inputs.length);
    }

    public double[] getInputs() {
        return inputs;
    }

    public double[] getDeltas() {
        return deltas;
    }

    public AbstractFunction[] getActivationFunctions() {
        return activationFunctions;
    }

    public double[] getOutputs() {
        return outputs;
    }

    public double[][] getWeights() {
        return weights;
    }

    public void setWeights(double[] weights, int from) {
        int bound = this instanceof Type1Layer ? inputs.length : inputs.length + 1;
        for (int i = 0; i < bound; ++i) {
            for (int j = 0; j < outputs.length; ++j) {
                this.weights[i][j] = weights[from++];
            }
        }
    }

    public double[] getNetSums() {
        return netSums;
    }

    public void calculateOutputs() {

        for (int j = 0; j < outputs.length; ++j) {
            netSums[j] = 0;
        }

        for (int i = 0; i <= inputs.length; ++i) {
            for (int j = 0; j < outputs.length; ++j) {
                netSums[j] += (i < inputs.length ? inputs[i] : -1) * weights[i][j];
            }
        }

        for (int i = 0; i < outputs.length; ++i) {
            outputs[i] = activationFunctions[i].calculate(netSums[i]);
        }
    }

    public Layer deepCopy() {
        Layer clone = new Layer();

        clone.inputs = Utils.copyArrayOfDoubles(inputs);
        clone.outputs = Utils.copyArrayOfDoubles(outputs);
        clone.netSums = Utils.copyArrayOfDoubles(netSums);
        clone.deltas = Utils.copyArrayOfDoubles(deltas);
        clone.activationFunctions = new AbstractFunction[activationFunctions.length];
        if (weights != null) {
            clone.weights = new double[inputs.length + 1][outputs.length];
            for (int i = 0; i < inputs.length + 1; ++i) {
                clone.weights[i] = Utils.copyArrayOfDoubles(weights[i]);
            }
        }

        System.arraycopy(
                activationFunctions, 0,
                clone.activationFunctions, 0,
                activationFunctions.length
        );

        return clone;
    }

    @Override
    public String toString() {
        return Arrays.toString(outputs);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Layer layer = (Layer) o;
        return Arrays.equals(weights, layer.weights) &&
                Arrays.equals(activationFunctions, layer.activationFunctions);
    }

    @Override
    public int hashCode() {
        int result = Arrays.hashCode(weights);
        result = 31 * result + Arrays.hashCode(activationFunctions);
        return result;
    }

}