package lab5.neuro.neuroskeleton.layer;

import lab5.function.AbstractFunction;
import lab5.function.concrete.Linear;

public class LinearLayer extends Layer {

    public LinearLayer(int neurons, int nextLayerNeurons) {
        super(neurons, nextLayerNeurons, new AbstractFunction[nextLayerNeurons]);

        for (int i = 0; i < nextLayerNeurons; ++i) {
            activationFunctions[i] = new Linear(1.0f, 0.0f);
        }
    }

}
