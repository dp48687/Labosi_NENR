package lab5.neuro.neuroskeleton.layer;

import lab5.function.AbstractFunction;
import lab5.function.concrete.Sigmoid;

public class SigmoidLayer extends Layer {

    public SigmoidLayer(int neurons, int nextLayerNeurons) {
        super(neurons, nextLayerNeurons, new AbstractFunction[nextLayerNeurons]);

        for (int i = 0; i < nextLayerNeurons; ++i) {
            activationFunctions[i] = new Sigmoid(1.0f, 0.0f);
        }
    }

}
