package lab5.neuro.neuroskeleton.layer;

import lab5.function.AbstractFunction;
import utils.various.Utils;

public class Type1Layer extends Layer {

    /**
     * Parameters of each neuron, used in output calculation.
     */
    private double[] normalizationFactors;

    /**
     * @param neurons
     * @param nextLayerNeurons
     */
    public Type1Layer(int neurons, int nextLayerNeurons) {
        init(neurons, nextLayerNeurons, null);
    }

    /**
     * @return normalization factors
     */
    public double[] getNormalizationFactors() {
        return normalizationFactors;
    }

    /**
     *
     * @param weights
     * @param weightCounter
     */
    public void setNormalizationFactors(double[] weights, int weightCounter) {
        for (int i = 0; i < normalizationFactors.length; ++i) {
            normalizationFactors[i] = weights[weightCounter + i];
        }
    }

    @Override
    protected void init(int neurons, int nextLayerNeurons, AbstractFunction[] activationFunctions) {
        normalizationFactors = new double[neurons * nextLayerNeurons];
        inputs = new double[neurons];
        outputs = new double[nextLayerNeurons];
        if (nextLayerNeurons > 0) {
            weights = new double[neurons][nextLayerNeurons];
        }
        assignRandomWeights(WEIGHTS_LOWER_BOUND, WEIGHTS_UPPER_BOUND);
    }

    @Override
    public void calculateOutputs() {
        for (int i = 0; i < outputs.length; ++i) {
            outputs[i] = 0;
            for (int j = 0; j < inputs.length; ++j) {
                outputs[i] += Math.abs(inputs[j] - weights[j][i]) / Math.abs(normalizationFactors[i * inputs.length + j]);
            }
            outputs[i] = 1.0 / (1 + outputs[i]);
        }
    }

    @Override
    public Layer deepCopy() {
        Type1Layer clone = new Type1Layer(inputs.length, outputs.length);

        clone.inputs = Utils.copyArrayOfDoubles(inputs);
        clone.outputs = Utils.copyArrayOfDoubles(outputs);
        clone.normalizationFactors = Utils.copyArrayOfDoubles(normalizationFactors);

        if (weights != null) {
            for (int i = 0; i < inputs.length; ++i) {
                clone.weights[i] = Utils.copyArrayOfDoubles(weights[i]);
            }
        }

        return clone;
    }

}
