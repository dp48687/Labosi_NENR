package lab5.neuro.trainingsamples;

import utils.various.FileIOUtils;
import utils.various.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Contains the functionality for loading and saving training samples as well as
 * splitting them into two sets: training and validation.
 */
public class SampleParser {

    public static List<TrainingSample> loadFromString(String contents) {
        String[] lines = contents.trim().split("\n");
        List<TrainingSample> samples = new ArrayList<>(lines.length);
        for (int i = 0; i < lines.length; ++i) {
            try {
                samples.add(TrainingSample.fromString(lines[i]));
            } catch (Exception e) {
                throw new RuntimeException(
                        "Bad lab1 encoding at line " + i + " (enumerated from 0th line)."
                );
            }
        }

        return samples;
    }

    public static List<TrainingSample> loadFromFile(String path) {
        return loadFromString(FileIOUtils.readFileContents(path));
    }

    public static void saveSamples(String savePath, List<TrainingSample> willBeSaved) {

        StringBuilder samplePoolBuilder = new StringBuilder();
        willBeSaved.forEach(sample -> {
            samplePoolBuilder.append(sample);
            samplePoolBuilder.append('\n');
        });
        samplePoolBuilder.deleteCharAt(samplePoolBuilder.length() - 1);


        FileIOUtils.writeToFile(savePath, samplePoolBuilder.toString());

    }

    @SuppressWarnings("unchecked")
    public static List<TrainingSample>[] split(List<TrainingSample> samples, int trainingSize, boolean shuffle) {
        if (shuffle) {
            Collections.shuffle(samples);
        }

        return new List[]{
                Utils.subList(samples, 0, trainingSize),
                Utils.subList(samples, trainingSize, samples.size())
        };
    }

}
