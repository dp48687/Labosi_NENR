package lab5.neuro.trainingsamples;

import utils.various.Utils;

import java.util.Arrays;

public class TrainingSample {

    private double[] inputs;
    private double[] outputs;

    public TrainingSample(double[] inputs, double[] outputs) {
        this.inputs = inputs;
        this.outputs = outputs;
    }

    public static TrainingSample fromString(String content) {
        String[] splitted = content.split("->");

        double[] inputs = Utils.parseArrayOfDoubles(splitted[0]);
        double[] outputs = Utils.parseArrayOfDoubles(splitted[1]);

        return new TrainingSample(inputs, outputs);
    }

    public double[] getOutputs() {
        return outputs;
    }

    public double[] getInputs() {
        return inputs;
    }

    public double inputAt(int index) {
        return inputs[index];
    }

    public double outputAt(int index) {
        return outputs[index];
    }

    @Override
    public String toString() {
        return Arrays.toString(inputs) + " -> " + Arrays.toString(outputs);
    }

}
