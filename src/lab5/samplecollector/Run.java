package lab5.samplecollector;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.io.IOException;

public class Run extends Application {

    @Override
    public void start(Stage primaryStage) throws IOException {
        GridPane pane = FXMLLoader.load(
                Run.class.getResource(
                        "/lab5/samplecollector/gui/sample_collector.fxml"
                )
        );
        Scene scene = new Scene(pane);
        primaryStage.setScene(scene);
        primaryStage.setTitle("");
        primaryStage.setResizable(false);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

}
