package lab5.samplecollector.gui;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import utils.various.FileIOUtils;
import utils.various.WindowLauncherService;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

public class SampleCollectorController implements Initializable {

    @FXML
    private GridPane root;

    @FXML
    private Canvas canvas;

    @FXML
    private Label wantedLetter;


    private boolean dontShowAdditionalWindow = false;
    private int currentTextIndex;
    private static final String[] POSSIBLE_TEXTS = new String[]{
            "TRAŽENO SLOVO: alfa",
            "TRAŽENO SLOVO: beta",
            "TRAŽENO SLOVO: gama",
            "TRAŽENO SLOVO: delta",
            "TRAŽENO SLOVO: epsilon"
    };

    private GraphicsContext context;
    private static final Color BACKGROUND_COLOR = Color.valueOf("#DBE8FF");
    private static final Color LINE_COLOR = Color.RED;

    private boolean drawingEnabled = true;
    private List<Double> xCoordinates = new ArrayList<>(500);
    private List<Double> yCoordinates = new ArrayList<>(500);

    private StringBuilder samplesBuilder = new StringBuilder();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        context = canvas.getGraphicsContext2D();
        context.setStroke(Color.RED);

        canvas.addEventHandler(MouseEvent.MOUSE_PRESSED, event -> {
            if (drawingEnabled) {
                context.beginPath();
                context.moveTo(event.getX(), event.getY());
                context.stroke();
                xCoordinates.add(event.getX());
                yCoordinates.add(event.getY());
                clearBackground();
                drawConnectedPoints();

            } else {
                alreadyPresentPoints();

            }
        });

        canvas.addEventHandler(MouseEvent.MOUSE_DRAGGED, event -> {
            if (drawingEnabled) {
                context.lineTo(event.getX(), event.getY());
                context.stroke();
                xCoordinates.add(event.getX());
                yCoordinates.add(event.getY());
                clearBackground();
                drawConnectedPoints();

            } else {

                alreadyPresentPoints();

            }
        });

        canvas.addEventHandler(MouseEvent.MOUSE_RELEASED, event -> drawingEnabled = false);

        clearBackground();
    }

    @FXML
    public void left() {
        save();

        --currentTextIndex;
        if (currentTextIndex < 0) {
            currentTextIndex = POSSIBLE_TEXTS.length - 1;
        }

        onSwiped();
    }

    @FXML
    public void right() {
        save();

        currentTextIndex = (++currentTextIndex) % POSSIBLE_TEXTS.length;

        onSwiped();
    }

    private void save() {
        String collected = collectDataAsText();
        if (collected.equals("")) {
            WindowLauncherService.launchSimpleErrorDialog(
                    "POGREŠKA",
                    "Niste ništa nacrtali!"
            );
            return;
        }

        if (samplesBuilder.toString().length() != 0) {
            samplesBuilder.append('\n');
        }
        samplesBuilder.append(collected);


        if (!dontShowAdditionalWindow) {
            final String stopShowing = "DA";
            String userSelected = WindowLauncherService.launchMultiSelectionDialog(
                    "INFORMACIJA",
                    "Slovo koje ste nacrtali registrirano je kao "
                            + POSSIBLE_TEXTS[currentTextIndex].replace("TRAŽENO SLOVO: ", "") + ".\n" +
                            "Kad ugasite program, bit će spremljeno u datoteku.\n" +
                            "Želite li da se ovaj prozor prestane prikazivati?",
                    Arrays.asList(
                            stopShowing,
                            "NE"
                    )
            );

            if (userSelected.equals(stopShowing)) {
                dontShowAdditionalWindow = true;
            }
        }

        delete();
    }

    @FXML
    public void delete() {
        xCoordinates.clear();
        yCoordinates.clear();
        drawingEnabled = true;

        clearBackground();
    }

    @FXML
    public void saveAndQuit() {
        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter(
                "TXT files", "txt"
        );
        chooser.setFileFilter(filter);
        int returnVal = chooser.showOpenDialog(null);
        if(returnVal == JFileChooser.APPROVE_OPTION) {
            String name = chooser.getSelectedFile().getAbsolutePath();
            FileIOUtils.writeToFile(name, samplesBuilder.toString());
            Platform.exit();
        }
    }

    private void onSwiped() {
        wantedLetter.setText(POSSIBLE_TEXTS[currentTextIndex]);
    }

    private void clearBackground() {
        Paint fillBefore = context.getFill();

        context.setFill(BACKGROUND_COLOR);
        context.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());

        context.setFill(fillBefore);
    }

    private void drawConnectedPoints() {
        Paint strokeBefore = context.getStroke();

        for (int i = 0; i < yCoordinates.size() - 1; ++i) {
            context.setStroke(i > 5 ? LINE_COLOR : Color.BLACK);
            context.strokeLine(
                    xCoordinates.get(i), yCoordinates.get(i),
                    xCoordinates.get(i + 1), yCoordinates.get(i + 1)
            );
        }

        context.setStroke(strokeBefore);
    }

    private void alreadyPresentPoints() {
        Alert alert = new Alert(
                Alert.AlertType.CONFIRMATION,
                "Na površini za crtanje već su zabilježene točke." +
                        "\nMožete spremiti podatak ili geneticalgorithm obrisati i tek onda će biti moguć unos novog slova." +
                        "\n\nObjašnjenje: slova moraju biti crtana u jednom potezu " +
                        "i onim smjerom kako je opisano u uputi koja je dostupna uz ovaj program!",
                ButtonType.YES
        );
        alert.setHeaderText("INFORMACIJA");
        alert.showAndWait();
    }

    private String collectDataAsText() {
        if (!yCoordinates.isEmpty()) {
            StringBuilder builder = new StringBuilder(yCoordinates.toString().length() * 2 + 19);

            builder.append('[');
            for (int i = 0; i < xCoordinates.size(); ++i) {
                builder.append(xCoordinates.get(i));
                builder.append(',');
                builder.append(' ');
                builder.append(yCoordinates.get(i));
                if (i < xCoordinates.size() - 1) {
                    builder.append(',');
                    builder.append(' ');
                }
            }
            builder.append(']');

            builder.append(" -> [");
            for (int i = 0; i < POSSIBLE_TEXTS.length; ++i) {
                builder.append(currentTextIndex != i ? '0' : '1');
                if (i < POSSIBLE_TEXTS.length - 1) {
                    builder.append(", ");
                }
            }
            builder.append(']');

            return builder.toString();

        } else {
            return "";

        }
    }

}
