package lab6;

import lab5.neuro.trainingsamples.SampleParser;
import lab5.neuro.trainingsamples.TrainingSample;

import java.util.ArrayList;
import java.util.List;

public class DataGenerator {

    private static final int LOWER_BOUND_INCLUSIVE = -4;

    private static final int UPPER_BOUND_INCLUSIVE = 4;


    private DataGenerator() {

    }


    public static List<TrainingSample> next() {
        int quantity =  (UPPER_BOUND_INCLUSIVE - LOWER_BOUND_INCLUSIVE + 1) *
                        (UPPER_BOUND_INCLUSIVE - LOWER_BOUND_INCLUSIVE + 1);

        List<TrainingSample> samples = new ArrayList<>(quantity);

        for (int i = LOWER_BOUND_INCLUSIVE; i <= UPPER_BOUND_INCLUSIVE; ++i) {
            for (int j = LOWER_BOUND_INCLUSIVE; j <= UPPER_BOUND_INCLUSIVE; ++j) {
                samples.add(
                        new TrainingSample(
                                new double[]{i, j},
                                new double[]{calculateOutput(i, j)}
                                )
                );
            }
        }

        return samples;
    }

    private static double calculateOutput(int x, int y) {
        double cos = Math.cos((double) x / 5);
        return ((x - 1) * (x - 1) + (y + 2) * (y + 2) - 5 * x * y + 3) * cos * cos;
    }

    public static void main(String[] args) {
        List<TrainingSample> samples = next();
        SampleParser.saveSamples("src/lab6/resources/samples.txt", samples);
    }

}