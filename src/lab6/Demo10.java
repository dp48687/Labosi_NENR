package lab6;

import lab5.neuro.trainingsamples.SampleParser;
import lab5.neuro.trainingsamples.TrainingSample;
import lab6.anfis.Anfis;
import lab6.learning.AbstractAnfisLearningAlgorithm;
import lab6.learning.concrete.OnlineAnfisLearning;
import utils.various.FileIOUtils;
import utils.various.StringUtils;

import java.util.Arrays;
import java.util.List;


public class Demo10 {

    public static void main(String[] args) {

        List<TrainingSample> samples = SampleParser.loadFromFile(
                FileIOUtils.pathJoin(new String[]{"src", "lab6", "resources", "samples.txt"})
        );

        for (int numberOfRules: Arrays.asList(1, 2, 4, 6, 10)) {
            Anfis anfisNetwork = new Anfis(numberOfRules, 2);

            AbstractAnfisLearningAlgorithm algorithm = new OnlineAnfisLearning(
                    anfisNetwork,
                    samples
            );

            StringBuilder anfiss = new StringBuilder(anfisNetwork.toString().length() * 10);
            StringBuilder resultsBuilder = new StringBuilder(1000 * 95);
            for (int i = 0; i < 300000; ++i) {

                double mse = algorithm.processSingleEpoch();
                if (i % 10000 == 0) {
                    System.out.println("N_rules: " + numberOfRules + ", epoch: " + i + ", mse: " + mse);
                    anfiss.append("EPOCH " + i + "\n");
                    anfiss.append(anfisNetwork);
                    anfiss.append("\n\n");
                }

                String res = "EPOCH: " + (i + 1) +
                        "     MEAN SQUARED ERROR: " + mse +
                        "     MEAN ABSOLUTE ERROR: " + Math.sqrt((mse * samples.size())) / samples.size();
                resultsBuilder.append(res);

                resultsBuilder.append("\n\n\n");
            }

            String res = printAbsoluteDifferences(anfisNetwork, samples);

            FileIOUtils.writeToFile(
                    FileIOUtils.pathJoin(new String[]{
                            "src", "lab6", "results", "trained_anfis (" + numberOfRules + ").txt"
                    }),
                    anfiss.toString()
            );
            FileIOUtils.writeToFile(
                    FileIOUtils.pathJoin(new String[]{
                            "src", "lab6", "results", "results(" + numberOfRules + ").txt"
                    }),
                    resultsBuilder.toString()
            );
            FileIOUtils.writeToFile(
                    FileIOUtils.pathJoin(new String[]{
                            "src", "lab6", "results", "calculated(" + numberOfRules + ").txt"
                    }),
                    res
            );
        }

    }

    private static String printAbsoluteDifferences(Anfis net, List<TrainingSample> samples) {
        StringBuilder res = new StringBuilder();

        samples.forEach(s -> {
            double calculatedOutput = net.calculateOutput(s.getInputs());
            double y = s.getOutputs()[0];
            res.append(StringUtils.formatNumber(y, "#0.00000"));
            res.append("   ");
            res.append(StringUtils.formatNumber(calculatedOutput, "#0.00000"));
            res.append("\n\n");
            System.out.println(Math.abs(y - calculatedOutput));
        });

        return res.toString();
    }

}
