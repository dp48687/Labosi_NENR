package lab6.anfis;

import utils.various.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class Anfis {

    /**
     *
     */
    private List<Rule> rules;

    /**
     *
     * @param numberOfRules
     */
    public Anfis(int numberOfRules, int numberOfInputs) {
        this.rules = new ArrayList<>(numberOfRules);

        for (int i = 0; i < numberOfRules; ++i) {
            rules.add(new Rule(numberOfInputs));
        }
    }

    /**
     *
     * @param rules
     */
    public Anfis(List<Rule> rules) {
        this.rules = rules;
    }

    /**
     * @param inputVector
     * @return
     */
    public double calculateOutput(double[] inputVector) {
        double sumOfWeights = calculateSumOfWeights(inputVector);
        double weightedSum = 0;

        for (Rule r: rules) {
            weightedSum += r.calculateFunction(inputVector) * r.calculateWeight(inputVector);
        }

        return weightedSum / sumOfWeights;
    }

    /**
     *
     * @param inputVector
     * @return
     */
    public double calculateSumOfWeights(double[] inputVector) {
        double sumOfWeights = 0;

        for (Rule r: rules) {
            sumOfWeights += r.calculateWeight(inputVector);
        }

        return sumOfWeights;
    }

    /**
     * Retrieves the rules.
     * @return rules
     */
    public List<Rule> getRules() {
        return rules;
    }

    /**
     * Parses parameters from string and returns ANFIS network which has those
     * parameters.
     * @param content
     * @return
     */
    public static Anfis fromString(String content) {
        String[] parameters = content.split("\n");

        List<Rule> parsedRules = new ArrayList<>(parameters.length / 2);
        for (int i = 0; i < parameters.length; i += 2) {
            parsedRules.add(
                    new Rule(
                            Utils.parseArrayOfDoubles(parameters[i]),
                            Utils.parseArrayOfDoubles(parameters[i + 1])
                    )
            );
        }

        return new Anfis(parsedRules);
    }

    public static Anfis dummyNetwork(int numberOfRules, int numberOfInputs) {
        float beforeLB = Rule.PARAMETERS_LOWER_BOUND;
        float beforeUB = Rule.PARAMETERS_UPPER_BOUND;

        Rule.PARAMETERS_LOWER_BOUND = 1;
        Rule.PARAMETERS_UPPER_BOUND = 1;

        Anfis dummy = new Anfis(numberOfRules, numberOfInputs);

        Rule.PARAMETERS_LOWER_BOUND = beforeLB;
        Rule.PARAMETERS_UPPER_BOUND = beforeUB;

        return dummy;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(
                rules.get(0).toString().length() + rules.size() * 2 + 2
        );

        for (int i = 0; i < rules.size(); ++i) {
            builder.append(rules.get(i));
            if (i < rules.size() - 1) {
                builder.append('\n');
            }
        }

        return builder.toString();
    }

}