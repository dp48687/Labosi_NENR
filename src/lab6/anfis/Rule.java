package lab6.anfis;

import utils.random.RandomNumberGenerator;

import java.util.Arrays;

/**
 *
 * @author danci
 * @version 1.0
 */
public class Rule {

    /**
     *
     */
    public static float PARAMETERS_LOWER_BOUND = -0.5f;
    public static float PARAMETERS_UPPER_BOUND = 0.5f;

    /**
     *
     */
    private double[] parameters;

    /**
     *
     */
    private double[] sigmoidParameters;

    /**
     * @param size number of inputs
     */
    public Rule(int size) {
        initialize(size);
    }

    /**
     *
     * @param parameters
     * @param sigmoidParameters
     */
    public Rule(double[] parameters, double[] sigmoidParameters) {
        this.parameters = parameters;
        this.sigmoidParameters = sigmoidParameters;
    }

    /**
     * Initializes all parameters with double values from interval [0, 1].
     */
    private void initialize(int size) {
        parameters = RandomNumberGenerator.nextDoubleVector(
                size + 1, PARAMETERS_LOWER_BOUND, PARAMETERS_UPPER_BOUND
        );
        sigmoidParameters = RandomNumberGenerator.nextDoubleVector(
                2 * size, PARAMETERS_LOWER_BOUND, PARAMETERS_UPPER_BOUND
        );
    }

    /**
     *
     * @param steepness
     * @param shift
     * @param arg
     * @return
     */
    public double sigmoid(double steepness, double shift, double arg) {
        return 1.0 / (1 + Math.exp(steepness * (arg - shift)));
    }

    /**
     *
     * @param inputVector vector of input values
     * @return
     */
    public double calculateWeight(double[] inputVector) {
        double accumulated = 1;

        for (int i = 0; i < inputVector.length; ++i) {
            accumulated *= sigmoid(sigmoidParameters[2 * i], sigmoidParameters[2 * i + 1], inputVector[i]);
        }

        return accumulated;
    }

    /**
     *
     * @param inputVector vector of input values
     * @return
     */
    public double calculateFunction(double[] inputVector) {
        double accumulated = 0;

        for (int i = 0; i < inputVector.length; ++i) {
            accumulated += inputVector[i] * parameters[i];
        }

        return accumulated + parameters[parameters.length - 1];
    }

    /**
     *
     * @return
     */
    public double[] getParameters() {
        return parameters;
    }

    /**
     *
     * @return
     */
    public double[] getSigmoidParameters() {
        return sigmoidParameters;
    }

    @Override
    public String toString() {
        return Arrays.toString(parameters) + "\n" + Arrays.toString(sigmoidParameters);
    }

}