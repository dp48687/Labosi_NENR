package lab6.learning;

import lab5.neuro.trainingsamples.TrainingSample;
import lab6.anfis.Anfis;
import lab6.anfis.Rule;

import java.util.ArrayList;
import java.util.List;

/**
 * The anfis learning algorithm which performs mapping R^2 -> R.
 */
public abstract class AbstractAnfisLearningAlgorithm {

    /**
     *
     */
    protected double[][] sigmoidParametersCorrections;

    /**
     *
     */
    protected double[][] parametersCorrections;

    /**
     *
     */
    protected double[] sigmoidEvaluations;

    /**
     *
     */
    private double[] functionEvaluations;

    /**
     *
     */
    private double[] weightEvaluations;

    /**
     *
     */
    private double learningRateSigmoidParameters = 0.0005;

    /**
     *
     */
    private double learningRateParameters = 0.002;


    /**
     *
     */
    protected Anfis willBeTrained;

    /**
     *
     */
    protected List<TrainingSample> samples;

    /**
     * Stores MSE for each iteration of the algorithm.
     */
    private List<Double> msePerEpoch = new ArrayList<>(10_000);

    /**
     *
     * @param learningRate
     * @param willBeTrained
     * @param samples
     */
    public AbstractAnfisLearningAlgorithm(double learningRate,
                                          Anfis willBeTrained,
                                          List<TrainingSample> samples) {
        this.willBeTrained = willBeTrained;
        this.samples = samples;

        functionEvaluations = new double[willBeTrained.getRules().size()];
        weightEvaluations = new double[functionEvaluations.length];
    }

    /**
     *
     * @param willBeTrained
     * @param samples
     */
    public AbstractAnfisLearningAlgorithm(Anfis willBeTrained, List<TrainingSample> samples) {
        this(1.0 / (2 * samples.size()), willBeTrained, samples);
    }

    /**
     *
     */
    protected void updateParameters() {

        for (int i = 0; i < willBeTrained.getRules().size(); ++i) {
            Rule r = willBeTrained.getRules().get(i);

            double[] ruleParameters = r.getParameters();
            for (int j = 0; j < ruleParameters.length; ++j) {
                ruleParameters[j] -= learningRateParameters * parametersCorrections[i][j];
                parametersCorrections[i][j] = 0;
            }

            double[] ruleSigmoidParameters = r.getSigmoidParameters();
            for (int j = 0; j < ruleSigmoidParameters.length; ++j) {
                ruleSigmoidParameters[j] -= learningRateSigmoidParameters * sigmoidParametersCorrections[i][j];
                sigmoidParametersCorrections[i][j] = 0;
            }
        }

    }

    /**
     * Calculates function values and weights.
     * @param sample
     */
    private void makePrecalculations(TrainingSample sample) {
        for (int i = 0; i < willBeTrained.getRules().size(); ++i) {
            Rule rule = willBeTrained.getRules().get(i);
            functionEvaluations[i] = rule.calculateFunction(sample.getInputs());
            weightEvaluations[i] = rule.calculateWeight(sample.getInputs());
        }
    }

    /**
     *
     * @param indexOfWeight
     * @param sumOfWeights
     * @return
     */
    private double calculateWeightedDifferenceAverage(int indexOfWeight, double sumOfWeights) {
        double weightedDifference = 0;
        for (int j = 0; j < willBeTrained.getRules().size(); ++j) {
            if (indexOfWeight != j) {
                weightedDifference += weightEvaluations[j] * (functionEvaluations[indexOfWeight] - functionEvaluations[j]);
            }
        }
        return weightedDifference / sumOfWeights;
    }

    /**
     *
     * @param j
     * @return
     */
    private double jthSigmoidProduct(int j) {
        double sigmoidProduct = 1;

        for (int i = 0; i < sigmoidEvaluations.length; ++i) {
            if (i != j) {
                sigmoidProduct *= sigmoidEvaluations[i];
            }
        }

        return sigmoidProduct;
    }

    /**
     * Calculates corrections based on derivations.
     * DOES NOT update model parameters.
     *
     * @param sample sample used in training process
     * @return squared difference between the expected and calculated output
     */
    protected double calculateCorrections(TrainingSample sample) {

        if (parametersCorrections == null) {
            int size = sample.getInputs().length;

            parametersCorrections = new double[willBeTrained.getRules().size()][size + 1];
            sigmoidParametersCorrections = new double[willBeTrained.getRules().size()][size * size];
            sigmoidEvaluations = new double[size];
        }

        double calculatedOutput = willBeTrained.calculateOutput(sample.getInputs());

        for (int i = 0; i < willBeTrained.getRules().size(); ++i) {
            calculateCorrections(i, sample, calculatedOutput);
        }

        return (sample.getOutputs()[0] - calculatedOutput) * (sample.getOutputs()[0] - calculatedOutput);
    }

    /**
     *
     * @param ruleIndex
     * @param sample
     * @param calculatedOutput
     */
    private void calculateCorrections(int ruleIndex, TrainingSample sample, double calculatedOutput) {

        makePrecalculations(sample);

        Rule rule = willBeTrained.getRules().get(ruleIndex);
        double sumOfWeights = willBeTrained.calculateSumOfWeights(sample.getInputs());
        double[] sigmoidParameters = rule.getSigmoidParameters();
        double[] parameters = rule.getParameters();

        for (int i = 0; i < sigmoidEvaluations.length; ++i) {
            sigmoidEvaluations[i] = rule.sigmoid(
                    sigmoidParameters[2 * i],
                    sigmoidParameters[2 * i + 1],
                    sample.getInputs()[i]
            );
        }

        double errorByOutput = deriveErrorByOutput(sample.getOutputs()[0], calculatedOutput);
        double weightedDiffAverage = calculateWeightedDifferenceAverage(ruleIndex, sumOfWeights);

        for (int i = 0; i < parameters.length; ++i) {
            parametersCorrections[ruleIndex][i] +=
                                    errorByOutput *
                                    deriveFByFi(ruleIndex, sumOfWeights) *
                                    deriveFiByPj(i, sample);
        }

        for (int i = 0; i < sigmoidParametersCorrections[0].length; ++i) {

            int sigmoidEvaluationIndex = i / 2;

            if (i % 2 == 0) {
                sigmoidParametersCorrections[ruleIndex][i] +=
                                errorByOutput *
                                weightedDiffAverage *
                                deriveWeightBySigmoidSteepness(
                                        rule,
                                        i,
                                        sigmoidEvaluationIndex,
                                        jthSigmoidProduct(ruleIndex),
                                        sample.getInputs()[sigmoidEvaluationIndex]
                                );
            } else {
                sigmoidParametersCorrections[ruleIndex][i] +=
                                errorByOutput *
                                weightedDiffAverage *
                                deriveWeightBySigmoidShift(
                                        rule, i, sigmoidEvaluationIndex, jthSigmoidProduct(ruleIndex)
                                );
            }

        }

    }

    /**
     *
     * @param y
     * @param calculatedOutput
     * @return
     */
    private double deriveErrorByOutput(double y, double calculatedOutput) {
        return -(y - calculatedOutput);
    }

    /**
     *
     * @param parameterIndex
     * @param weightSum
     * @return
     */
    private double deriveFByFi(int parameterIndex, double weightSum) {
        return weightEvaluations[parameterIndex] / weightSum;
    }

    /**
     *
     * @param parameterIndex
     * @param sample
     * @return
     */
    private double deriveFiByPj(int parameterIndex, TrainingSample sample) {
        return parameterIndex < sample.getInputs().length ? sample.getInputs()[parameterIndex] : 1;
    }

    /**
     *
     * @param indexOfWeight
     * @param weightedDifferenceAverage
     * @return
     */
    private double deriveOutputByWeight(int indexOfWeight, double weightedDifferenceAverage) {
        return weightEvaluations[indexOfWeight] * weightedDifferenceAverage;
    }

    /**
     *
     * @param r
     * @param parameterIndex
     * @param jthSigmoidProduct
     * @param input
     * @return
     */
    private double deriveWeightBySigmoidSteepness(Rule r,
                                                  int parameterIndex,
                                                  int sigmoidEvaluationIndex,
                                                  double jthSigmoidProduct,
                                                  double input) {
        return jthSigmoidProduct * (r.getSigmoidParameters()[parameterIndex] - input) *
                sigmoidEvaluations[sigmoidEvaluationIndex] *
                (1 - sigmoidEvaluations[sigmoidEvaluationIndex]);
    }

    /**
     * Sigmoid derived by parameter a
     *
     * @return derivation
     */
    private double deriveWeightBySigmoidShift(Rule r,
                                              int parameterIndex,
                                              int sigmoidEvaluationIndex,
                                              double jthSigmoidProduct) {
        return jthSigmoidProduct * (r.getSigmoidParameters()[parameterIndex]) *
                sigmoidEvaluations[sigmoidEvaluationIndex] *
                (1 - sigmoidEvaluations[sigmoidEvaluationIndex]);
    }

    /**
     *
     */
    protected void registerError(double mse) {
        msePerEpoch.add(mse);
    }

    /**
     * Processes the single iteration of learning algorithm.
     * @return accumulated error in this epoch
     */
    public abstract double processSingleEpoch();

}