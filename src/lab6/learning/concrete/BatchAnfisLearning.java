package lab6.learning.concrete;

import lab5.neuro.trainingsamples.TrainingSample;
import lab6.anfis.Anfis;
import lab6.learning.AbstractAnfisLearningAlgorithm;

import java.util.Collections;
import java.util.List;

/**
 *
 * @author Danijel Pavlek
 * @version 1.0
 */
public class BatchAnfisLearning extends AbstractAnfisLearningAlgorithm {

    /**
     *
     * @param willBeTrained
     * @param samples
     */
    public BatchAnfisLearning(Anfis willBeTrained, List<TrainingSample> samples) {
        super(willBeTrained, samples);
    }

    @Override
    public double processSingleEpoch() {
        double accumulatedMSE = 0;

        for (TrainingSample sample : samples) {
            accumulatedMSE += calculateCorrections(sample);
        }
        updateParameters();
        Collections.shuffle(samples);

        accumulatedMSE /= samples.size();
        registerError(accumulatedMSE);

        return accumulatedMSE;
    }

}