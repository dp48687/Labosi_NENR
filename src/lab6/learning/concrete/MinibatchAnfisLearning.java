package lab6.learning.concrete;

import lab5.neuro.trainingsamples.TrainingSample;
import lab6.anfis.Anfis;
import lab6.learning.AbstractAnfisLearningAlgorithm;

import java.util.List;

/**
 *
 * @author Danijel Pavlek
 * @version 1.0
 */
public class MinibatchAnfisLearning extends AbstractAnfisLearningAlgorithm {

    /**
     *
     */
    private int batchSize;

    /**
     *
     * @param willBeTrained
     * @param samples
     */
    public MinibatchAnfisLearning(Anfis willBeTrained, List<TrainingSample> samples, int batchSize) {
        super(willBeTrained, samples);

        if (batchSize > 1 && batchSize < samples.size() / 2) {
            this.batchSize = batchSize;
        } else {
            throw new IllegalArgumentException(
                    "Batch size must be an integer from interval [2, " + samples.size() / 2 + ">." +
                    " However, you provided: " + batchSize
            );
        }

    }

    @Override
    public double processSingleEpoch() {
        double accumulatedMSE = 0;

        for (int i = 0; i < samples.size(); ++i) {
            accumulatedMSE += calculateCorrections(samples.get(i));
            if ((i + 1) % batchSize == 0) {
                updateParameters();
            }
        }

        accumulatedMSE /= samples.size();
        registerError(accumulatedMSE);

        return accumulatedMSE;
    }

}