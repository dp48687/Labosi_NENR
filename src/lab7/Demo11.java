package lab7;

import lab4.fitnessfunction.IFunction;
import lab4.geneticalgorithm.Chromosome;
import lab4.geneticalgorithm.GeneticAlgorithm;
import lab5.neuro.neuroskeleton.NeuralNetwork;
import utils.various.FileIOUtils;
import utils.various.StringUtils;

import java.io.File;
import java.util.Arrays;

/**
 * @author danci
 * @version 1.0
 */
public class Demo11 {

    /**
     *
     */
    private static NeuralNetwork ann = new NeuralNetwork("2x-6x4x3");

    /**
     *
     */
    private static GeneticAlgorithm decimalGA;

    /**
     *
     */
    private static String datasetPath = FileIOUtils.pathJoin(
            new String[]{"src", "lab7", "resources", "zad7-dataset.txt"}
    );

    /**
     *
     */
    private static double[][] dataset = loadDataset(datasetPath);

    /**
     *
     */
    private static final int NUMBER_OF_EPOCHS = 10_000;

    /**
     * Stores inputs.
     */
    private static double[] inputs = new double[2];

    /**
     * Fitness function calculates the mean squared error
     */
    private static IFunction fitnessFunction = weights -> {
        ann.assignWeights(weights);
        return calculateError();
    };

    public static void main(String[] args) {

        initGeneticAlgorithm();

        StringBuilder results = new StringBuilder(100000 * 70);
        for (int i = 0; i < NUMBER_OF_EPOCHS; ++i) {
            decimalGA.runGenerationVariation();
            String iterationFormatted = i + "." + StringUtils.nTimes(' ', 15 - Integer.toString(i).length());
            String fitnessValueFormatted = StringUtils.formatNumber(decimalGA.getTheBestFitness(), "#0.000000000");
            fitnessValueFormatted = fitnessValueFormatted + StringUtils.nTimes(
                    ' ', 20 - fitnessValueFormatted.length()
            );

            String ithResult = "EPOCH " + iterationFormatted +
                    "MSE: " + fitnessValueFormatted +
                    "CLASSIFICATION LOSS: " + getClassificationLoss();

            results.append(ithResult);
            results.append('\n');

            if (i % 1000 == 0) {
                System.out.println(Arrays.toString(decimalGA.getTheBestVariables()));
                System.out.println(ithResult);
            }

        }

        saveNN(FileIOUtils.pathJoin(new String[]{"src", "lab7", "results", "task6", "upgradedga", "net.txt"}));
        FileIOUtils.writeToFile(
                FileIOUtils.pathJoin(new String[]{"src", "lab7", "results", "task6", "upgradedga", "results_ga.txt"}),
                results.toString()
        );

    }

    /**
     *
     */
    private static void initGeneticAlgorithm() {
        String sep = File.separator;

        decimalGA = GeneticAlgorithm.fromString(
                FileIOUtils.readFileContents(
                        "src" + sep + "lab7" + sep + "resources" + sep + "ga_configuration_2.txt"
                )
        );
        decimalGA.attachFitnessFunction(fitnessFunction);

    }

    /**
     * @param path
     * @return
     */
    private static double[][] loadDataset(String path) {
        String[] loadedContentLines = FileIOUtils.readFileContents(path).split("\n");
        double[][] dataset = new double[loadedContentLines.length][5];

        for (int i = 0; i < loadedContentLines.length; ++i) {
            String[] splitted = loadedContentLines[i].split("\t");
            for (int j = 0; j < splitted.length - 2; ++j) {
                dataset[i][j] = Double.parseDouble(splitted[j]);
            }
        }

        return dataset;
    }

    /**
     * @return
     */
    private static double calculateError() {
        double accumulatedMSE = 0.0;
        int classificationLoss = 0;

        for (int i = 0; i < dataset.length; ++i) {
            inputs[0] = dataset[i][0];
            inputs[1] = dataset[i][1];
            double[] calculatedOutputs = ann.calculateOutputs(inputs);
            accumulatedMSE += calculateMSE(
                    dataset[i][2], dataset[i][3], dataset[i][4], calculatedOutputs
            );
            classificationLoss += calculateClassificationLoss(
                    dataset[i][2], dataset[i][3], dataset[i][4], calculatedOutputs
            );
        }

        return accumulatedMSE / dataset.length;
    }

    /**
     * @param y1
     * @param y2
     * @param y3
     * @param outputs
     * @return
     */
    private static double calculateMSE(double y1, double y2, double y3, double[] outputs) {
        return (y1 - outputs[0]) * (y1 - outputs[0]) +
                (y2 - outputs[1]) * (y2 - outputs[1]) +
                (y3 - outputs[2]) * (y3 - outputs[2]);
    }

    /**
     * @param y1
     * @param y2
     * @param y3
     * @param outputs
     * @return
     */
    private static int calculateClassificationLoss(double y1, double y2, double y3, double[] outputs) {
        return step(y1) != step(outputs[0]) ||
                step(y2) != step(outputs[1]) ||
                step(y3) != step(outputs[2]) ?
                1 : 0;
    }

    /**
     * Maps the provided value: 0 if value < 0.5, 1 otherwise
     *
     * @param value
     * @return
     */
    private static int step(double value) {
        return value < 0.5 ? 0 : 1;
    }

    /**
     * Prints the classification loss of the best individual.
     */
    private static int getClassificationLoss() {

        NeuralNetwork theBest = ann.deepCopy();
        theBest.assignWeights(decimalGA.getTheBestVariables());

        int classificationLoss = 0;

        for (int i = 0; i < dataset.length; ++i) {
            inputs[0] = dataset[i][0];
            inputs[1] = dataset[i][1];
            double[] calculatedOutputs = theBest.calculateOutputs(inputs);
            classificationLoss += calculateClassificationLoss(
                    dataset[i][2], dataset[i][3], dataset[i][4], calculatedOutputs
            );
        }

        return classificationLoss;
    }

    /**
     * Saves the neural network weights to the file.
     *
     * @param filePath
     */
    private static void saveNN(String filePath) {
        NeuralNetwork theBest = ann.deepCopy();
        theBest.assignWeights(decimalGA.getTheBestVariables());
        FileIOUtils.writeToFile(filePath, theBest);
    }

}
