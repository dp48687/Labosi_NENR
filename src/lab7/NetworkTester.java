package lab7;

import lab5.neuro.neuroskeleton.NeuralNetwork;
import utils.various.FileIOUtils;
import utils.various.StringUtils;

import java.io.File;

public class NetworkTester {

    private static final float STEP = 0.0005f;

    private static final String PATH_TO_SAVE_RESULTS = "src" + File.separator +
                                                        "lab7" + File.separator +
                                                        "results" + File.separator +
                                                        "task4" + File.separator +
                                                        "classicga" + File.separator +
                                                        "overall.txt";

    private static final String PATH_TO_NEURAL_NET = "src" + File.separator +
                                                     "lab7" + File.separator +
                                                     "results" + File.separator +
                                                     "task4" + File.separator +
                                                     "classicga" + File.separator +
                                                     "net.txt";

    public static void test(NeuralNetwork trainedNetwork) {
        double[] input = new double[2];
        double[] output = new double[3];
        StringBuilder builder = new StringBuilder(38000100);
        for (double x1 = 0; x1 < 1; x1 += STEP) {
            for (double x2 = 0; x2 < 1; x2 += STEP) {
                input[0] = x1;
                input[1] = x2;
                trainedNetwork.calculateIntoField(input, output);
                builder.append(StringUtils.formatNumber(x1, "#0.0000"));
                builder.append(' ');
                builder.append(StringUtils.formatNumber(x2, "#0.0000"));
                builder.append(' ');
                builder.append(StringUtils.formatNumber(output[0], "#0.00000"));
                builder.append(' ');
                builder.append(StringUtils.formatNumber(output[1], "#0.00000"));
                builder.append(' ');
                builder.append(StringUtils.formatNumber(output[2], "#0.00000"));
                builder.append('\n');
            }
        }

        FileIOUtils.writeToFile(PATH_TO_SAVE_RESULTS, builder.toString());

    }

    public static void main(String[] args) {
        test(NeuralNetwork.fromString(FileIOUtils.readFileContents(PATH_TO_NEURAL_NET)));
    }

}
