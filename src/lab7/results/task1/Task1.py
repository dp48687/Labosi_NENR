import matplotlib.pyplot as plt

if __name__ == "__main__":
    figure = plt.figure("TASK 1: Type 1 neuron", figsize=(10, 4.5))

    step = 0.001
    lb, ub = -10, 10
    domain = [x * step for x in range(int(lb / step), int(ub / step))]
    w = 2.0

    def y(x, s):
        return 1.0 / (1 + abs(x - w) / s)

    plt.grid(color='#000000', linestyle=':', linewidth=0.5)
    for s in (0.25, 1, 2, 4):
        plt.plot(domain, [y(x, s) for x in domain])
    plt.xlabel("Type 1 neuron, w = 2")
    plt.legend(("s=0.25", "s=1", "s=2", "s=4"))
    plt.show()
