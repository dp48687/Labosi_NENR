package utils.customexception;

public class NotImplementedException extends RuntimeException {

    public NotImplementedException() {
        super("This part of code is not implemented yet!");
    }

}
