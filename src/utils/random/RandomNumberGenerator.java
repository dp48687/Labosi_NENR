package utils.random;

import java.util.Random;

public class RandomNumberGenerator {

    public static Random random = new Random();

    private RandomNumberGenerator() {

    }

    public static double nextDoubleIn(double lowerBound, double upperBound) {
        return lowerBound + random.nextDouble() * (upperBound - lowerBound);
    }

    public static double[] nextDoubleVector(int size, double lowerBound, double upperBound) {
        double[] newVector = new double[size];

        for (int i = 0; i < size; ++i) {
            newVector[i] = nextDoubleIn(lowerBound, upperBound);
        }

        return newVector;
    }

    public static double nextGaussian() {
        return random.nextGaussian();
    }

    public static double nextGaussian(double mean, double standardDeviation) {
        return mean + standardDeviation * nextGaussian();
    }

    public static int nextIntIn(int lowerBound, int upperBound) {
        return lowerBound + Math.abs(random.nextInt()) % (upperBound - lowerBound + 1);
    }

}
