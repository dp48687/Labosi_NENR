package utils.various;

import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.*;
import java.util.Arrays;

/**
 *
 * @author Danijel Pavlek
 */
public class FileIOUtils {

    /**
     * This is how many bytes are read/written at once to/from a file.
     */
    public static final int BYTES_TO_RW_AT_ONCE = 512;

    /**
     *
     */
    private static FileChooser chooser;

    /**
     *
     * @param path
     * @return
     */
    public static String readFileContents(String path) {
        try {
            StringBuilder builder = new StringBuilder();

            InputStream is = new FileInputStream(path);
            int read;
            byte[] buffer = new byte[BYTES_TO_RW_AT_ONCE];
            while ((read = is.read(buffer)) > 0) {
                builder.append(
                        new String(
                                BYTES_TO_RW_AT_ONCE == read ?
                                        buffer :
                                        Arrays.copyOfRange(buffer, 0, read)
                        )
                );
            }

            return builder.toString().replace("\r\n", "\n");
        } catch (FileNotFoundException badPath) {
            throw new RuntimeException(
                    "Path which you have provided does not exist or it does not represent a file: '" + path + "'!"
            );

        } catch (IOException readingError) {
            throw new RuntimeException(
                    "There was an error during reading from file."
            );

        }
    }

    /**
     *
     * @param savePath
     * @param contentToSave
     */
    public static void writeToFile(String savePath, Object contentToSave) {
        try {
            OutputStream os = new FileOutputStream(savePath);
            byte[] contentToWrite = contentToSave.toString().getBytes();
            int offset = 0;
            while (offset < contentToWrite.length) {
                int write = offset + BYTES_TO_RW_AT_ONCE <= contentToWrite.length ?
                        BYTES_TO_RW_AT_ONCE :
                        contentToWrite.length - offset;
                os.write(contentToWrite, offset, write);
                offset += BYTES_TO_RW_AT_ONCE;
            }

        } catch (FileNotFoundException invalidPath) {
            throw new RuntimeException("A bad path provided: '" + savePath + "'!");

        } catch (IOException writingError) {
            throw new RuntimeException("Error happened while writing to file!");

        }
    }

    /**
     *
     * @param stage
     * @return
     */
    public static String selectFilePath(Stage stage) {
        if (chooser == null) {
            chooser = new FileChooser();
            chooser.getExtensionFilters().add(
                    new FileChooser.ExtensionFilter(
                            "Text Files (*.txt)", "*.txt"
                    )
            );
        }
        File file = chooser.showOpenDialog(stage);
        return file != null ? file.getAbsolutePath() : null;
    }

    /**
     *
     * @param pathChunks
     * @return
     */
    public static String pathJoin(String[] pathChunks) {
        String separator = File.separator;

        StringBuilder builder = new StringBuilder(50);
        for (int i = 0; i < pathChunks.length; ++i) {
            builder.append(pathChunks[i]);
            if (i < pathChunks.length - 1) {
                builder.append(separator);
            }
        }

        return builder.toString();
    }

}