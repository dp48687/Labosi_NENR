package utils.various;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Map;

public class StringUtils {

    /**
     *
     */
    private static Map<String, NumberFormat> formats = new HashMap<>();


    /**
     *
     * @param number
     * @param format
     * @return
     */
    public static String formatNumber(double number, String format) {
        if (!formats.containsKey(format)) {
            formats.put(format, new DecimalFormat(format));
        }

        return formats.get(format).format(number);
    }

    /**
     *
     * @param content
     * @param format
     * @return
     */
    public static String formatString(String content, String format) {
        return String.format(content, format);
    }

    /**
     *
     * @return
     */
    public static String nTimes(Object str, int n) {
        if (n == 0) {
            return "";
        } else if (n < 0) {
            throw new IllegalArgumentException(
                    "Invalid number of iterations given: " + n
            );
        } else {
            StringBuilder builder = new StringBuilder(str.toString().length() * n);
            for (int i = 0; i < n; ++i) {
                builder.append(str);
            }
            return builder.toString();
        }
    }

}
