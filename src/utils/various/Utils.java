package utils.various;

import java.util.ArrayList;
import java.util.List;

/**
 * Various set of functions.
 */
public class Utils {

    /**
     *
     * @param copyMe
     * @return
     */
    public static double[] copyArrayOfDoubles(double[] copyMe) {
        if (copyMe != null) {
            double[] clone = new double[copyMe.length];
            System.arraycopy(
                    copyMe, 0,
                    clone, 0,
                    copyMe.length
            );
            return clone;
        } else {
            throw new NullPointerException(
                    "Cannot copy a null-field!"
            );
        }
    }

    /**
     * Parses the array which is represented using String in the form which returns
     * Arrays.toString(array)
     * @param content content which will be parsed
     * @return the parsed array of doubles
     */
    public static double[] parseArrayOfDoubles(String content) {
        String[] strContent = content.replace("]", "")
                .replace("[", "")
                .replace(" ", "")
                .split(",");

        double[] parsed = new double[strContent.length];
        for (int i = 0; i < parsed.length; ++i) {
            parsed[i] = Double.parseDouble(strContent[i]);
        }

        return parsed;
    }

    /**
     *
     * @param list
     * @param fromInclusive
     * @param toExclusive
     * @return
     */
    public static <T> List<T> subList(List<T> list, int fromInclusive, int toExclusive) {
        if (fromInclusive > toExclusive) {
            throw new RuntimeException(
                    "Bad bounds provided: [" + fromInclusive + ", " + toExclusive + ">"
            );
        }

        List<T> result = new ArrayList<>(toExclusive - fromInclusive);

        for (int i = fromInclusive; i < toExclusive; ++i) {
            try {
                result.add(list.get(i));
            } catch (IndexOutOfBoundsException outOfList) {
                throw new RuntimeException(
                                "Check indices when calling this method: " +
                                "List size: " + list.size() + ", " +
                                "bounds: [" + fromInclusive + ", " + toExclusive + ">"
                );

            }
        }

        return result;
    }

    /**
     * Calculates the squared error between two arrays of double
     * elements.
     * @param array1 first array
     * @param array2 second array
     * @return squared difference
     */
    public static double calculateSquaredDifference(double[] array1, double[] array2) {
        double mse = 0;

        for (int i = 0; i < array1.length; ++i) {
            mse += (array1[i] - array2[i]) * (array1[i] - array2[i]);
        }

        return mse;
    }

    /**
     *
     * @param data
     * @return
     */
    public static double max(double[] data) {
        double max = data[0];

        for (int i = 1; i < data.length; ++i) {
            max = Math.max(max, data[i]);
        }

        return max;
    }

    /**
     *
     * @param data
     * @return
     */
    public static int argmax(double[] data) {
        double max = data[0];
        int indexOfMax = 0;

        for (int i = 1; i < data.length; ++i) {
            if (data[i] > max) {
                max = data[i];
                indexOfMax = i;
            }
        }

        return indexOfMax;
    }

}