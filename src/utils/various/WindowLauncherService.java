package utils.various;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class WindowLauncherService {

    private WindowLauncherService() {

    }

    public static String launchMultiSelectionDialog(String title,
                                                    String promptText,
                                                    List<String> buttonTexts) {
        Alert alert = new Alert(
                Alert.AlertType.CONFIRMATION
        );
        alert.setTitle(title);
        alert.setHeaderText("");
        alert.setContentText(promptText);

        List<ButtonType> types = new ArrayList<>(buttonTexts.size());
        buttonTexts.forEach(bt -> types.add(new ButtonType(bt)));
        alert.getButtonTypes().setAll(types.toArray(new ButtonType[0]));

        Optional<ButtonType> result = alert.showAndWait();

        for (ButtonType t : types) {
            if (result.get().equals(t)) {
                return t.getText();
            }
        }

        throw new IllegalArgumentException(
                "An unexpected bug happened!"
        );
    }

    public static void launchSimpleErrorDialog(String title, String errorExplanation) {
        launchAlert(title, errorExplanation, Alert.AlertType.WARNING);
    }

    public static void launchSimpleInformationDialog(String title, String informationDetails) {
        launchAlert(title, informationDetails, Alert.AlertType.INFORMATION);
    }

    private static void launchAlert(String title, String errorExplanation, Alert.AlertType type) {
        Alert alert = new Alert(type);
        alert.setTitle(title);
        alert.setHeaderText("");
        alert.setContentText(errorExplanation);

        alert.showAndWait();
    }

    public static String launchTextDialog(String promptText) {
        boolean resultPresent = false;
        while (!resultPresent) {
            try {
                TextInputDialog dialog = new TextInputDialog("Tran");

                dialog.setTitle("UNESITE TEKST");
                dialog.setHeaderText(promptText);
                dialog.setContentText("");

                resultPresent = true;
                return dialog.showAndWait().get();
            } catch (NullPointerException e) {

            }
        }
        return null;
    }

    public static void getBack(Pane root) {
        ((Stage) (root.getScene().getWindow())).close();
    }

}